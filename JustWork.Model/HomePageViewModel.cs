﻿using System.Collections.Generic;
using JustWork.Logic.DTO;
using JustWork.Model.OrdersViewModels;

namespace JustWork.Model
{
    public class HomePageViewModel
    {
        public List<OrderViewModel> OrderViewModels { get; set; }
        public List<RatingDTO> RatingDtos { get; set; }
        public int OrdersCount { get; set; }
        public int CustomerCount { get; set; }
        public int RatingsCount { get; set; }
        public int EmployersCount { get; set; }
    }
}