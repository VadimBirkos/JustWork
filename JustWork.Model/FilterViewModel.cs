﻿namespace JustWork.Model
{
    public class FilterViewModel
    {
        public string Criterion { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
    }
}