﻿using System.ComponentModel.DataAnnotations;

namespace JustWork.Model.UserModels
{
    public class ConcactViewModel
    {
        [Required]
        [MaxLength(20, ErrorMessage = "Name should be contains less than 20 characters")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [MinLength(5, ErrorMessage = "Email should be contains more than 5 characters")]
        [MaxLength(30, ErrorMessage = "Email should be contains less than 30 characters")]
        public string UserAdress { get; set; }

        [Required]
        [MinLength(5, ErrorMessage = "Message should be contains more than 5 characters")]
        [MaxLength(300, ErrorMessage = "Message should be contains less than 300 characters")]
        public string Message { get; set; }
    }
}