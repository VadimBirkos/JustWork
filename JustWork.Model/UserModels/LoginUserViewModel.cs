﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JustWork.Model.UserModels
{
    public class LoginUserViewModel
    {
        [Required]
        [Remote("CheckNameOnExistForLogin", "User", ErrorMessage = "User with this login or email not exist")]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
