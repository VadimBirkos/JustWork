﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JustWork.Model.UserModels
{
    public class EditEmailViewModel
    {
        [Required]
        [EmailAddress(ErrorMessage = "This is not valid e-mail adress.")]
        [DisplayName("E-mail")]
        [Remote("CheckEmailOnExist", "User", ErrorMessage = "Email is already taken")]
        [MaxLength(30, ErrorMessage = "Email should be contains less than 30 characters")]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}