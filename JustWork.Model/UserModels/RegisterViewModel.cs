﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JustWork.Model.UserModels
{
    public class RegisterViewModel
    {
        [Required]
        [Remote("CheckNameOnExist","User",ErrorMessage = "User name is already taken")]
        [MinLength(4, ErrorMessage = "User name should be contains more than 4 characters")]
        [MaxLength(30, ErrorMessage = "User name should be contains less than 30 characters")]
        [RegularExpression(@"([a-zA-Z0-9 .&'-]+)", ErrorMessage = "Enter only alphabets and numbers of Login")]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [RegularExpression("([a-zA-Z0-9 .&'-]+).{6,20}$", 
            ErrorMessage = "Invalid password format. The password should contain latin and numeric characters.")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Passwords is not an equal")]
        [MinLength(6, ErrorMessage = "Password should be contains more than 6 characters")]
        [MaxLength(30, ErrorMessage = "Password should be contains less than 30 characters")]
        [Display(Name = "Password confirm")]
        public string PasswordConfirm { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "This is not valid e-mail adress.")]
        [DisplayName("E-mail")]
        [Remote("CheckEmailOnExist", "User", ErrorMessage = "Email is already taken")]
        [MaxLength(25, ErrorMessage = "Email should be contains less than 20 characters")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Account type")]
        public int RoleId { get; set; }
    }
}