﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using JustWork.Logic.DTO;

namespace JustWork.Model.UserModels
{
    public class ProfileViewModel
    {
        [HiddenInput]
        public int AccountId { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        [MinLength(4, ErrorMessage = "User name should be contains more than 4 characters")]
        [MaxLength(20, ErrorMessage = "User name should be contains less than 12 characters")]
        public string Name { get; set; }

        [DataType(DataType.ImageUrl)]
        public ImageDTO Avatar { get; set; }

        public UserDTO UserDto { get; set; }

        [Display(Name = "About yourself")]
        [DataType(DataType.MultilineText)]
        [MaxLength(400, ErrorMessage = "About yourself should be contains less than 12 characters")]
        public string AdditionalInfo { get; set; }

        [Display(Name = "Your birthday")]
        [DisplayFormat(DataFormatString = "{0:MM.dd.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime BirthDate { get; set; }

        public PortfolioDTO Portfolio { get; set; }

        [Display(Name = "Service types")]
        public ICollection<ServiceTypeDTO> ServiceTypes { get; set; }

        public ICollection<UserPaidDTO> UserPaids { get; set; }

        public int NewOrdersCount { get; set; }
        public int InProgressOrdersCount { get; set; }
        public int CompleteOrdersCount { get; set; }
        public int CancelOrderCount { get; set; }
    }
}