﻿using System.Collections.Generic;
using JustWork.Logic.DTO;

namespace JustWork.Model
{
    public class WorkersViewModel
    {
        public IEnumerable<AccountDTO> Workers { get; set; }
        public PageInfoViewModel PageInfoViewModel { get; set; }    
    }
}