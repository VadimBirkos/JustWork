﻿using System.Collections.Generic;

namespace JustWork.Model.OrdersViewModels
{
    public class OrderIndexViewModel
    {
        public List<OrderViewModel> OrderList { get; set; }
        public PageInfoViewModel PageInfo { get; set; }
    }
}