﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JustWork.Model.OrdersViewModels
{
    public class OrderFinishViewModel
    {
        [HiddenInput]
        public int Id { get; set; }

        [Required]
        public int OrderStateId { get; set; }

        [Required]
        [Range(1, 5, ErrorMessage = "Rating can has value between 1 and 5.")]
        [Display(Name = "Rating", Description ="Rating")]
        public double RatingValue { get; set; }

        [Required]
        [MaxLength(100, ErrorMessage = "Message is very big.")]
        public string Message { get; set; }
    }
}