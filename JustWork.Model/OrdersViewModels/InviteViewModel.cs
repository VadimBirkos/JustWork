﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JustWork.Model.OrdersViewModels
{
    public class InviteViewModel
    {
        [HiddenInput]
        public int OrderId { get; set; }

        [Required]
        [MinLength(20, ErrorMessage = "Invite description should be bigger than 20.")]
        [MaxLength(300, ErrorMessage = "Invite description should be less than 300.")]
        public string Description { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Cost should be greater than 0.")]
        public double Cost { get; set; }
    }
}