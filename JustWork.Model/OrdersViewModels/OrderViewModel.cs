﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using JustWork.Logic.DTO;
using System.Web;

namespace JustWork.Model.OrdersViewModels
{
    public class OrderViewModel : IValidatableObject
    {
        public OrderViewModel()
        {
            StartDate = DateTime.Now;
            EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(1);
        }

        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Required]
        [MinLength(5, ErrorMessage = "Order name should be contains more than 5 characters.")]
        [MaxLength(40, ErrorMessage = "Order name should be contains less than 40 characters.")]
        public string Name { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:#.###}", ApplyFormatInEditMode = true)]
        [Range(1, 1000000, ErrorMessage = "Budget should be greater than 0 and less than 1 000 000.")]
        public double Budget { get; set; }

        [Required]
        [Display(Name = "Order description")]
        [DataType(DataType.MultilineText)]
        [MaxLength(1100, ErrorMessage = "Order name should be contains less than 1100 characters" +".")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Start date")]
        [DisplayFormat(DataFormatString = "{0:MM.dd.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [Required]
        [Display(Name = "End date")]
        [DisplayFormat(DataFormatString = "{0:MM.dd.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }
        
        [Display(Name = "Order state")]
        public OrderStateDTO OrderStateDto { get; set; }

        [ScaffoldColumn(false)]
        public AccountDTO Employer { get; set; }
        [ScaffoldColumn(false)]
        public AccountDTO Customer { get; set; }

        public ICollection<OrderInviteDTO> OrderInvites { get; set; }
        public ICollection<RatingDTO> RatingsDto { get; set; }

        public HttpPostedFileBase ImageFile { get; set; }
        public string ImagePath { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (EndDate < StartDate)
            {
                yield return new ValidationResult(errorMessage: "End date must be greater, then start date.", memberNames:
                    new[] { "End Date" });
            }
        }
    }
}