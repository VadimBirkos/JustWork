﻿using System.Web.Optimization;

namespace JustWork.Website
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region defaultCss&Js
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/bootstrap.css", new CssRewriteUrlTransform()));
            #endregion

            #region datePicker
            bundles.Add(new StyleBundle("~/bundles/datePickerCss")
               .Include("~/Content/themes/css/jquery-ui.css", new CssRewriteUrlTransform()));

            bundles.Add(new ScriptBundle("~/bundles/datePickerJs")
                .Include("~/Scripts/jquery-ui-1.12.1.js"));
            #endregion

            #region jqeuryStars
            bundles.Add(new ScriptBundle("~/bundles/jqueryStarsjs")
                .Include("~/Content/WebControls/jqueryStars/js/star-rating.js"));

            bundles.Add(new StyleBundle("~/bundles/jqueryStarscss")
                .Include("~/Content/WebControls/jqueryStars/css/star-rating.css", new CssRewriteUrlTransform()));
            #endregion

            #region xEditable
            bundles.Add(new StyleBundle("~/bundles/xeditablecss")
                .Include("~/Content/WebControls/jqueryui-editable/css/jqueryui-editable.css", new CssRewriteUrlTransform()));

            bundles.Add(new ScriptBundle("~/bundles/xeditablejs")
                .Include("~/Content/WebControls/jqueryui-editable/js/jqueryui-editable.js"));

            #endregion

            #region mainPage

            bundles.Add(new StyleBundle("~/bundles/mainPageCss")
                .Include("~/Content/css/jasny-bootstrap.min.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/bootstrap-select.min.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/material-kit.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/font-awesome.min.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/themify-icons.css", new CssRewriteUrlTransform())
                .Include("~/Content/extras/animate.css", new CssRewriteUrlTransform())
                .Include("~/Content/extras/owl.carousel.css", new CssRewriteUrlTransform())
                .Include("~/Content/extras/owl.theme.css", new CssRewriteUrlTransform())
                .Include("~/Content/extras/settings.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/slicknav.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/main.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/responsive.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/colors/lightgreen.css", new CssRewriteUrlTransform()));


            bundles.Add(new ScriptBundle("~/bundles/mainPageJs")
                .Include("~/Scripts/theme/material.min.js",
               "~/Scripts/theme/material-kit.js",
               "~/Scripts/theme/jquery.parallax.js",
               "~/Scripts/theme/owl.carousel.min.js",
               "~/Scripts/theme/jquery.slicknav.js",
               "~/Scripts/theme/jquery.counterup.min.js",
               "~/Scripts/theme/waypoints.min.js",
               "~/Scripts/theme/jasny-bootstrap.min.js",
               "~/Scripts/theme/bootstrap-select.min.js",
               "~/Scripts/theme/main.js",
               "~/Scripts/theme/form-validator.min.js",
               "~/Scripts/theme/contact-form-script.js"));
            #endregion

            bundles.Add(new StyleBundle("~/Content/errorPages")
                .Include("~/Content/errorHandling.css", new CssRewriteUrlTransform()));

            bundles.Add(new ScriptBundle("~/bundles/profileScripts")
                .Include("~/Scripts/profileScripts.js"));

            bundles.Add(new ScriptBundle("~/bundles/order")
                .Include("~/Scripts/orderCreateScripts.js"));

            bundles.Add(new ScriptBundle("~/bundles/order")
                .Include("~/Scripts/orderCreateScripts.js"));

            bundles.Add(new ScriptBundle("~/bundles/imageScripts")
                .Include("~/Scripts/imageScripts.js"));

            bundles.Add(new ScriptBundle("~/bundles/indexOrder")
                .Include("~/Scripts/indexOrder.js"));

            bundles.Add(new ScriptBundle("~/bundles/invitesScripts")
                .Include("~/Scripts/invitesScripts.js"));

            bundles.Add(new ScriptBundle("~/bundles/commonScripts")
                .Include("~/Scripts/commonScripts.js"));
            
        }
    }
}
