﻿using System.Linq;
using AutoMapper;
using JustWork.Logic.AutoMapperProfiles;
using JustWork.Website.Infastructure.MapperProfile;

namespace JustWork.Website
{
    public static class AutoMapperConfigurator
    {
        public static void Configure()
        {
            var bllConfig = BllCommonCongif.ConfigureBllMapper();
            var plConfig = PlCommonConfig.ConfigurePlMapper();
            var resultConfig = bllConfig.Union(plConfig);

            Mapper.Initialize(cfg =>
            cfg.AddProfiles(resultConfig));
        }
    }
}