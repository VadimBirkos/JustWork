using System.Web.Mvc;
using JustWork.Common.Infrastructure.Logging;
using JustWork.Website.Infastructure.NinjectConfig;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(JustWork.Website.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(JustWork.Website.App_Start.NinjectWebCommon), "Stop")]

namespace JustWork.Website.App_Start
{
    using System;
    using System.Web;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using JustWork.Logic.NinjectConfig;
    using JustWork.DataProvider.NinjectConfig;
    using Ninject.Web.Common;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                RegisterLogger(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            DependencyResolver.SetResolver(new BllNinjectConfig(kernel));
            DependencyResolver.SetResolver(new DalNinjectConfig(kernel));
            DependencyResolver.SetResolver(new PlNinjectConfig(kernel));
        }

        private static void RegisterLogger(IKernel kernel)
        {
            kernel.Bind<ILoggerFactory>().To<SerilogLoggerFactory>();
            var loggerFactory = kernel.GetService(typeof(ILoggerFactory)) as ILoggerFactory;
            kernel.Bind<ILogger>().ToConstant(loggerFactory.CreateLogger());
        }   
    }
}
