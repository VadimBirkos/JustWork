﻿using System.Collections.Generic;

namespace JustWork.Website.Infastructure.Interfaces
{
    public interface ISearchService<TViewModel>
    {
        TViewModel Search(int? page);
    }
}