﻿using System;
using System.Linq.Expressions;
using JustWork.DataProvider.DalModels;
using JustWork.Model;
using JustWork.Model.OrdersViewModels;

namespace JustWork.Website.Infastructure.Interfaces
{
    public interface IOrderSearchService
    {
        OrderIndexViewModel Search(string search, int? page, FilterViewModel[] filters, Expression<Func<Order, bool>> addPredicate);
        OrderIndexViewModel SearchWithInviteSort(Expression<Func<Order, bool>> addPredicate, int? page);
        OrderIndexViewModel Search(string search, int? page, FilterViewModel[] filters);
    }
}