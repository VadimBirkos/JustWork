﻿using System.Threading.Tasks;

namespace JustWork.Website.Infastructure.Interfaces
{
    public interface IEmailNotifer
    {
        Task SendNotificationAsync(string email, string body);
    }
}