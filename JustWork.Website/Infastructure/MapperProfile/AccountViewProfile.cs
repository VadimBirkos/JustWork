﻿using AutoMapper;
using JustWork.Logic.DTO;
using JustWork.Model.UserModels;

namespace JustWork.Website.Infastructure.MapperProfile
{
    public class AccountViewProfile : Profile
    {
        public AccountViewProfile()
        {
            CreateMap<AccountDTO, ProfileViewModel>()
                .ForMember(dest => dest.AccountId,
                    opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.ServiceTypes,
                    opts => opts.MapFrom(src => src.ServiceTypesDto))
                .ForMember(dest => dest.Portfolio,
                    opts => opts.MapFrom(src => src.PortfolioDto))
                .ForMember(dest => dest.UserPaids,
                    opts => opts.MapFrom(src => src.UserPaidsDto));
        }
    }
}