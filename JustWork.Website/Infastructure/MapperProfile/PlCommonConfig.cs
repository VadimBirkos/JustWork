﻿using System;
using System.Collections.Generic;

namespace JustWork.Website.Infastructure.MapperProfile
{
    public class PlCommonConfig
    {
        public static List<Type> ConfigurePlMapper()
        {
            return new List<Type>
            {
                typeof(AccountViewProfile),
                typeof(OrderProfile),
                typeof(WorkerShowProfile)
            };
        }
    }
}