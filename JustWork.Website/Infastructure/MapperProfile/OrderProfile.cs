﻿using AutoMapper;
using JustWork.Logic.DTO;
using JustWork.Model.OrdersViewModels;

namespace JustWork.Website.Infastructure.MapperProfile
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateMap<OrderDTO, OrderViewModel>()
                .AfterMap((src, dest) =>
                    dest.Customer.UserDto = src.Customer.UserDto)
                .AfterMap((src, dest) =>
                {
                    if (src.Employer != null)
                    {
                        dest.Employer.UserDto = src.Employer.UserDto;
                    }
                })
                .AfterMap((src, dest) =>
                    dest.OrderInvites = src.OrderInvites)
                .AfterMap((src, dest) =>
                    dest.Customer.RatingsDto = src.Customer.RatingsDto)
                .AfterMap((src, dest) =>
                {
                    dest.ImagePath = src.ImageDto?.ImagePath;
                });

        CreateMap<OrderViewModel, OrderDTO>()
                .AfterMap((src, dest) =>
                dest.Customer = src.Customer)
                .AfterMap((src, dest) =>
                dest.Employer = src.Employer);
        }
    }
}