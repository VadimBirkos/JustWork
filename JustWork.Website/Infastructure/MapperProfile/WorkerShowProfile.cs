﻿using AutoMapper;
using JustWork.Logic.DTO;
using JustWork.Model;

namespace JustWork.Website.Infastructure.MapperProfile
{
    public class WorkerShowProfile:Profile
    {
        public WorkerShowProfile()
        {
            CreateMap<AccountDTO, WorkersViewModel>();
        }
    }
}