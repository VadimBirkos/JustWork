﻿using System.Collections.Generic;
using AutoMapper;
using JustWork.Logic.DTO;
using JustWork.Model.OrdersViewModels;

namespace JustWork.Website.Infastructure.Mappers
{
    public static class OrderMapper
    {
        public static OrderDTO MapViewToDtoModel(OrderViewModel viewModel)
        {
            return Mapper.Map<OrderViewModel, OrderDTO>(viewModel);
        }

        public static OrderViewModel MapDtoModelToView(OrderDTO dtoModel)
        {
            return Mapper.Map<OrderDTO, OrderViewModel>(dtoModel);
        }

        public static IEnumerable<OrderViewModel> MapToOrderLIstViewModel(List<OrderDTO> orders)
        {
            return Mapper.Map<List<OrderDTO>,List<OrderViewModel>>(orders);
        }
    }
}