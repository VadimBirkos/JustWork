﻿using AutoMapper;
using JustWork.Logic.DTO;
using JustWork.Model;
using JustWork.Model.UserModels;

namespace JustWork.Website.Infastructure.Mappers
{
    public static class AccountMapper
    {
        public static ProfileViewModel MapToProfileViewModel(AccountDTO accountDto)
        {
            var showAccModel = Mapper.Map<AccountDTO, ProfileViewModel>(accountDto);
            return showAccModel;
        }
    }
}