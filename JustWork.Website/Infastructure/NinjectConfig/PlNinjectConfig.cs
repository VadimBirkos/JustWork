﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using JustWork.Model;
using JustWork.Website.Infastructure.Interfaces;
using Ninject;

namespace JustWork.Website.Infastructure.NinjectConfig
{
    public class PlNinjectConfig:IDependencyResolver
    {
        private readonly IKernel _kernel;

        public PlNinjectConfig(IKernel kernel)
        {
            _kernel = kernel;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return _kernel.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            _kernel.Bind<IOrderSearchService>().To<OrderSearchService>();
            _kernel.Bind<ISearchService<WorkersViewModel>>().To<WorkerSearchService>();
            _kernel.Bind<IEmailNotifer>().To<EmailNotifer>();
        }
    }
}