﻿using System.Net.Mail;
using JustWork.Website.Infastructure.Interfaces;
using System.Threading.Tasks;

namespace JustWork.Website.Infastructure
{
    public class EmailNotifer:IEmailNotifer
    {
        public async Task SendNotificationAsync(string email, string body)
        {
            using (var smtp = new SmtpClient())
            {
                var m = new MailMessage(
                    new MailAddress("justworktech2017@gmail.com", "Just Work Team"),
                    new MailAddress(email))
                {
                    Subject = "Order Update",
                    Body = "Hello,<br>" + body+ "<br> Best Regards, <br> Just Work Team.",
                    IsBodyHtml = true
                };
                await smtp.SendMailAsync(m);
            }
        }
    }
}