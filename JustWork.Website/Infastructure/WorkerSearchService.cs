﻿using System;
using System.Linq;
using System.Linq.Expressions;
using JustWork.Common;
using JustWork.DataProvider.DalModels;
using JustWork.Logic.Interface;
using JustWork.Model;
using JustWork.Website.Infastructure.Interfaces;

namespace JustWork.Website.Infastructure
{
    public class WorkerSearchService:ISearchService<WorkersViewModel>
    {
        private Expression<Func<Account, bool>> _resultPredicate = PredicateBuilder.True<Account>();
        private IAccountService _accountService;

        public WorkerSearchService(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public WorkersViewModel Search(int? page)
        {
            var currentPage = (page.HasValue && page > 0) ? page.Value : 1;
            _resultPredicate = _resultPredicate.And(acc => acc.User.RoleId == (int) UserRoles.Employer);
            var searchResult = _accountService.GetByPredicate(_resultPredicate, currentPage);
            var resultWorers = searchResult.Item1.ToList();
            var totalCount = searchResult.Item2;

            var pageInfo = new PageInfoViewModel
            {
                PageNumber = currentPage,
                PageSize = ConfigurationsConstants.ItemCountOnPage,
                TotalItems = totalCount
            };

            var workersViewModel = new WorkersViewModel
            {
                Workers = resultWorers,
                PageInfoViewModel = pageInfo
            };

            return workersViewModel;
        }
    }
}