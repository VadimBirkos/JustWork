﻿using System.Text;
using System.Web.Mvc;
using JustWork.Model;

namespace JustWork.Website.Infastructure
{
    public static class PagingHelpers
    {
        //TODO make this condition with pattern for clean code
        public static MvcHtmlString PageLinks(this HtmlHelper html,
            PageInfoViewModel pageInfo)
        {
            var result = new StringBuilder();
            var currentPageNumber = pageInfo.PageNumber;
            var totalPages = pageInfo.TotalPages;

            if (currentPageNumber > 2)
            {
                MakeFirstLink(result);
            }

            if (currentPageNumber > 1)
            {
                var tag = BuildTag(pageInfo, currentPageNumber - 1);
                result.Append(tag);
            }
            var currentPage = BuildTag(pageInfo, currentPageNumber);
            result.Append(currentPage);

            if (currentPageNumber < totalPages)
            {
                var tag = BuildTag(pageInfo, currentPageNumber + 1);
                result.Append(tag);
            }

            if (currentPageNumber < totalPages - 1)
            {
                MakeLastLink(pageInfo, result);
            }

            return MvcHtmlString.Create(result.ToString());
        }

        private static TagBuilder BuildTag(PageInfoViewModel pageInfo, int i)
        {
            var listTag = new TagBuilder("li");
            var tag = new TagBuilder("a") { InnerHtml = i.ToString() };
            tag.MergeAttribute("data-pageSize", i.ToString());

            if (i == pageInfo.PageNumber)
            {
                listTag.AddCssClass("active");
            }
            else
            {
                tag.AddCssClass("commonSortBtn");
            }
            tag.AddCssClass("btn btn-common");
            listTag.InnerHtml += tag;
            return listTag;
        }

        private static void MakeLastLink(PageInfoViewModel pageInfo, StringBuilder result)
        {
            var icon = new TagBuilder("i");
            icon.AddCssClass("ti-angle-double-right");
            var listTag = new TagBuilder("li");
            var tag = new TagBuilder("a") { InnerHtml = "Last" + icon};
            tag.MergeAttribute("data-pageSize", pageInfo.TotalPages.ToString());
            tag.AddCssClass("common");
            tag.AddCssClass("btn btn-common");
            tag.AddCssClass("commonSortBtn");
            listTag.InnerHtml += tag;
            result.Append(listTag);
        }

        private static void MakeFirstLink(StringBuilder result)
        {
            var icon = new TagBuilder("i");
            icon.AddCssClass("ti-angle-double-left");
            var listTag = new TagBuilder("li");
            var tag = new TagBuilder("a") { InnerHtml = icon + "First" };
            tag.AddCssClass("btn btn-common");
            tag.AddCssClass("commonSortBtn");
            tag.MergeAttribute("data-pageSize", "1");
            listTag.InnerHtml += tag;
            result.Append(listTag);
        }


    }
}