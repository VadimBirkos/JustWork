﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using JustWork.Common;
using JustWork.DataProvider.DalModels;
using JustWork.Logic.Interface;
using JustWork.Model;
using JustWork.Model.OrdersViewModels;
using JustWork.Website.Infastructure.Interfaces;
using JustWork.Website.Infastructure.Mappers;
using static System.Data.Entity.Core.Objects.EntityFunctions;

namespace JustWork.Website.Infastructure
{
    public class OrderSearchService : IOrderSearchService
    {
        private Expression<Func<Order, bool>> _resultPredicate = PredicateBuilder.True<Order>();
        private Expression<Func<Order, bool>> _costPredicate = PredicateBuilder.True<Order>();
        private Expression<Func<Order, bool>> _durationPredicate = PredicateBuilder.True<Order>();
        private Expression<Func<Order, bool>> _searchPredicate = PredicateBuilder.True<Order>();
        private Expression<Func<Order, bool>> _statePredicate = PredicateBuilder.True<Order>();

        private readonly IOrderService _orderService;

        public OrderSearchService(
            IOrderService orderService
        )
        {
            _orderService = orderService;
        }

        public OrderIndexViewModel Search(string search, int? page, FilterViewModel[] filters)
        {
            var currentPage =(page.HasValue && page > 0) ? page.Value : 1;
            if (filters != null && filters.Length > 0)
            {
                FilterOrder(filters);
            }

            if (!string.IsNullOrEmpty(search))
            {
                var searchVal = search.ToLower();
                _searchPredicate = _searchPredicate.And(i => i.Name.ToLower().Contains(searchVal) || i.Description.ToLower().Contains(searchVal)
                || i.Customer.Name.ToLower().Contains(searchVal) || i.Employer.Name.ToLower().Contains(searchVal) 
                || i.Customer.User.Login.ToLower().Contains(searchVal) || i.Employer.User.Login.ToLower().Contains(searchVal));
            }

            _resultPredicate = _resultPredicate.And(_costPredicate);
            _resultPredicate = _resultPredicate.And(_durationPredicate);
            _resultPredicate = _resultPredicate.And(_searchPredicate);
            _resultPredicate = _resultPredicate.And(_statePredicate);

            var searchResult = _orderService.GetByPredicate(_resultPredicate, currentPage);
            var resultOrders = searchResult.Item1.ToList();
            var totalCount = searchResult.Item2;

            var pageInfo = new PageInfoViewModel
            {
                PageNumber = currentPage,
                PageSize = ConfigurationsConstants.ItemCountOnPage,
                TotalItems = totalCount
            };

            var models = OrderMapper.MapToOrderLIstViewModel(resultOrders).ToList();
            var orderViewModel = new OrderIndexViewModel
            {
                OrderList = models,
                PageInfo = pageInfo
            };
            return orderViewModel;
        }

        public OrderIndexViewModel Search(string search, int? page, FilterViewModel[] filters,
            Expression<Func<Order, bool>> addPredicate)
        {
            if (addPredicate == null) return Search(search, page, filters);
            _resultPredicate = _resultPredicate.And(addPredicate);
            return Search(search, page, filters);
        }

        public OrderIndexViewModel Search(Expression<Func<Order, bool>> addPredicate)
        {
            _resultPredicate = _resultPredicate.And(addPredicate);
            return Search(null, 1, null);
        }

        public OrderIndexViewModel SearchWithInviteSort(Expression<Func<Order, bool>> addPredicate, int? page)
        {
            var currentPage = page ?? 1;
          
            var searchResult = _orderService.SortByInvites(addPredicate, currentPage);
            var resultOrders = searchResult.Item1.ToList();
            var totalCount = searchResult.Item2;

            var pageInfo = new PageInfoViewModel
            {
                PageNumber = currentPage,
                PageSize = ConfigurationsConstants.ItemCountOnPage,
                TotalItems = totalCount
            };

            var models = OrderMapper.MapToOrderLIstViewModel(resultOrders).ToList();
            var orderViewModel = new OrderIndexViewModel
            {
                OrderList = models,
                PageInfo = pageInfo
            };
            return orderViewModel;
        }

        private void FilterOrder(IEnumerable<FilterViewModel> filters)
        {
            CheckOnCriterionExist(filters);
            foreach (var filter in filters)
            {
                if (filter.Criterion == "duration")
                {
                    _durationPredicate = _durationPredicate.Or(
                        i => i.EndDate >= AddDays(i.StartDate, filter.MinValue)
                             && i.EndDate <= AddDays(i.StartDate, filter.MaxValue));
                }
                else if (filter.Criterion == "cost")
                {
                    _costPredicate = _costPredicate.Or(i => i.Budget > filter.MinValue && i.Budget < filter.MaxValue);
                }
                else if (filter.Criterion == "state")
                {
                    _statePredicate = _statePredicate.Or(order => order.OrderStateId == filter.MinValue);
                }
            }
        }

        private void CheckOnCriterionExist(IEnumerable<FilterViewModel> filters)
        {
            if (filters.Count(i => i.Criterion == "duration") > 0)
            {
                _durationPredicate = PredicateBuilder.False<Order>();
            }

            if (filters.Count(i => i.Criterion == "cost") > 0)
            {
                _costPredicate = PredicateBuilder.False<Order>();
            }

            if (filters.Count(i => i.Criterion == "state") > 0)
            {
                _statePredicate = PredicateBuilder.False<Order>();
            }
        }
    }
}