﻿using System.IO;
using System.Web;
using System.Web.Mvc;

namespace JustWork.Website.Infastructure
{
    public static class HtmlHelperExtensions
    {
        public static string ImageOrDefaultAvatar(this HtmlHelper helper, string filename)
        {
            var imagePath = filename;
            var imageSrc = File.Exists(HttpContext.Current.Server.MapPath(imagePath))
                               ? imagePath : DefaultImage;

            return imageSrc;
        }

        private const string DefaultImage = "/Images/guest.png";
    }
}