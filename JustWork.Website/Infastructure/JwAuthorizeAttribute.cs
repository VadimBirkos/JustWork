﻿using System.Web.Mvc;
using System.Web.Routing;

namespace JustWork.Website.Infastructure
{
    public class JwAuthorizeAttribute:AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new 
                {
                    controller = "Error",
                    action = "AccessDenied",
                    StatusCode = 403
                }));
                filterContext.HttpContext.Response.StatusCode = 403;
            }
            
        }
    }
}