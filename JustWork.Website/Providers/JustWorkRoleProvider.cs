﻿using System.Web.Mvc;
using System.Web.Security;
using JustWork.Common.Exceptions;
using JustWork.Logic.DTO;
using JustWork.Logic.Interface;

namespace JustWork.Website.Providers
{
    public class JustWorkRoleProvider : RoleProvider
    {
        private readonly IService<RoleDTO> _roleService;
        private readonly IUserService _userService;

        public JustWorkRoleProvider()
        {
            _roleService = DependencyResolver.Current.GetService<IService<RoleDTO>>();
            //  _userService = DependencyResolver.Current.GetService<IUserService>();
        }

        public JustWorkRoleProvider(IService<RoleDTO> roleService, IUserService userService)
        {
            _roleService = roleService;
            _userService = userService;
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            var user = GetUserByName(username);
            if (CheckUserRoleByNull(user))
            {
                return false;
            }
            return user.RoleDto.Title == roleName;
        }

        public override void CreateRole(string roleName)
        {
            _roleService.Create(new RoleDTO { Title = roleName });
        }

        public override string[] GetRolesForUser(string username)
        {
            var user = GetUserByName(username);
            if (CheckUserRoleByNull(user))
            {
                throw new ItemNotFoundException("User role not found.");
            }

            return new[] { user.RoleDto.Title };
        }

        private UserDTO GetUserByName(string username)
        {
            var userService = DependencyResolver.Current.GetService<IUserService>();
            return userService.GetByLogin(username);
        }

        private static bool CheckUserRoleByNull(UserDTO user)
        {
            return user?.RoleDto == null;
        }

        #region Stumbs

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new System.NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new System.NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new System.NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new System.NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new System.NotImplementedException();
        }

        public override string ApplicationName { get; set; }
        #endregion
    }
}