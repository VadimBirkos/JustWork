﻿using System;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Mvc;
using JustWork.Common;
using JustWork.Logic.Interface;
using JustWork.Model;
using JustWork.Model.UserModels;
using JustWork.Website.Infastructure.Mappers;

namespace JustWork.Website.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly IOrderService _orderService;
        private readonly IRatingService _ratingService;

        public HomeController(
            IOrderService orderService,
            IRatingService ratingService,
            IAccountService accountService
            )
        {
            _orderService = orderService;
            _ratingService = ratingService;
            _accountService = accountService;
        }

        public ActionResult Index()
        {
            var orders = _orderService.SortByInvites(model => model.OrderStateId == (int)OrderStates.New, 1).Item1.ToList();

            var ratings = _ratingService.GetByPredicate(order => order.RatingValue >= 4).ToList();
            var ordersView = OrderMapper.MapToOrderLIstViewModel(orders).ToList();
            var homepageModel = new HomePageViewModel
            {
                OrderViewModels = ordersView,
                RatingDtos = ratings,
                RatingsCount = _ratingService.GetItemsCount(),
                OrdersCount = _orderService.GetItemsCount(),
                CustomerCount = _accountService.GetCount(acc => acc.User.RoleId == (int)UserRoles.Customer),
                EmployersCount = _accountService.GetCount(acc => acc.User.RoleId == (int)UserRoles.Employer)
            };
            return View(homepageModel);
        }

        [HttpGet]
        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Contact(ConcactViewModel emailModel)
        {
            using (var smtp = new SmtpClient())
            {
                var to = new MailAddress("vadim.birkos@gmail.com");
            var message = new MailMessage
            {
                Subject = "Just work feedback",
                Body = $"User {emailModel.Name}, {emailModel.UserAdress} wrote: \n{emailModel.Message}",
                From = new MailAddress(emailModel.UserAdress, emailModel.Name),
                IsBodyHtml = true
            };
            message.To.Add(to);
                await smtp.SendMailAsync(message);
            }
            return RedirectToAction("Index");
        }
    }
}