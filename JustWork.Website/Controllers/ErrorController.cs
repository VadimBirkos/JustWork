﻿using System.Web.Mvc;

namespace JustWork.Website.Controllers
{
    public class ErrorController : Controller
    {
        public new ActionResult HttpNotFound()
        {
            Response.StatusCode = 404;
            return PartialView("ErrorPages/HttpNotFound");
        }

        public  ActionResult InternalServerError()
        {
            Response.StatusCode = 500;
            return PartialView("ErrorPages/InternalServerError");
        }

        public ActionResult AccessDenied()
        {
            Response.StatusCode = 403;
            return PartialView("ErrorPages/AccessDeniedError");
        }
    }
}