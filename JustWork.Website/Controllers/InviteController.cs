﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using JustWork.Common;
using JustWork.Logic.DTO;
using JustWork.Logic.Interface;
using JustWork.Model.OrdersViewModels;
using JustWork.Website.Infastructure.Interfaces;

namespace JustWork.Website.Controllers
{
    public class InviteController : Controller
    {
        private readonly IOrderInviteService _inviteService;
        private readonly IAccountService _accountService;
        private readonly IEmailNotifer _emailNotificator;
        private readonly IOrderService _orderService;

        public InviteController(
            IOrderInviteService inviteService,
            IAccountService accountService, 
            IEmailNotifer emailNotificator, 
            IOrderService orderService
            )
        {
            _inviteService = inviteService;
            _accountService = accountService;
            _emailNotificator = emailNotificator;
            _orderService = orderService;
        }

        [HttpPost]
        [Authorize(Roles = "employer")]
        [ValidateInput(false)]
        public async Task<JsonResult> Add(InviteViewModel inviteModel)
        {
            var currentUser = _accountService.GetByUsernameWithInclude(User.Identity.Name);
            var order = _orderService.GetById(inviteModel.OrderId);
            if (order.OrderInvites.Any(orderInv => orderInv.Employer.Id == currentUser.Id))
            {
                return null; 
            }

            var invite = new OrderInviteDTO
            {
                Employer = currentUser,
                Message = inviteModel.Description,
                Cost = inviteModel.Cost,
                Order = new OrderDTO { Id = inviteModel.OrderId }
            };
            await NotifyAboutInvite(inviteModel.OrderId);
            _inviteService.Create(invite);
            return Json(invite);
        }

        private async Task NotifyAboutInvite(int orderId)
        {
            var order = _orderService.GetById(orderId);
            var orderLink = Url.Action("Show", "Order", new {id = orderId}, Request.Url.Scheme);
            if (!order.Customer.UserDto.ConfirmEmail) return;
            var body = ConfigurationsConstants.NewInviteMsg + $"<a href=\"{orderLink}\">Order Link</a>";
            await _emailNotificator.SendNotificationAsync(order.Customer.UserDto.Email, body);
        }

        [HttpGet]
        public ActionResult InviteShow(int id)
        {
            var invite = _inviteService.GetById(id);
            return PartialView("InviteViews/InviteListShow", new List<OrderInviteDTO>{ invite });
        }

        [HttpDelete]
        public ActionResult Delete(int inviteId)
        {
            var invite = _inviteService.GetById(inviteId);
            if (invite.Employer.UserDto.Login != User.Identity.Name)
            {
                return HttpNotFound();
            }
            _inviteService.Delete(invite);
            return new EmptyResult();
        }

        [HttpGet]
        [Authorize]
        public ActionResult LoadlIventForm()
        {
            return PartialView("InviteViews/NewInvite", new InviteViewModel());
        }
    }
}