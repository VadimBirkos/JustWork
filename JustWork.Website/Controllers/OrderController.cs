﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using AutoMapper;
using JustWork.Common;
using JustWork.Logic.DTO;
using JustWork.Logic.Interface;
using JustWork.Model;
using JustWork.Model.OrdersViewModels;
using JustWork.Website.Infastructure;
using JustWork.Website.Infastructure.Interfaces;
using JustWork.Website.Infastructure.Mappers;

namespace JustWork.Website.Controllers
{
    public class OrderController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly IOrderService _orderService;
        private readonly IOrderSearchService _orderSearchService;
        private readonly IRatingService _ratingService;
        private readonly IEmailNotifer _emailNotificator;

        public OrderController(
            IAccountService accountService,
            IOrderService orderService,
            IOrderSearchService orderSearchService,
            IRatingService ratingService,
            IEmailNotifer emailNotificator
            )
        {
            _accountService = accountService;
            _orderService = orderService;
            _orderSearchService = orderSearchService;
            _ratingService = ratingService;
            _emailNotificator = emailNotificator;
        }

        [HttpGet]
        public ActionResult Show(int id)
        {
            var order = _orderService.GetById(id);
            var viewModel = OrderMapper.MapDtoModelToView(order);
            return View(viewModel);
        }

        [HttpGet]
        [ValidateInput(false)]
        public ActionResult Index(string search, int? page, string criterion, int? maxValue, int? minValue)
        {
            var filter = new FilterViewModel();
            if (maxValue.HasValue && minValue.HasValue)
            {
                filter = new FilterViewModel
                {
                    Criterion = criterion,
                    MaxValue = maxValue.Value,
                    MinValue = minValue.Value
                };
            }
            var orders = _orderSearchService.Search(search, page, new[] { filter }, i => i.OrderStateId == (int)OrderStates.New);
            return View(orders);
        }

        [HttpGet, Authorize]
        public ActionResult Create()
        {
            return View(new OrderViewModel());
        }

        [HttpPost, Authorize]
        [ValidateInput(false)]
        public ActionResult Create(OrderViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var orderCustomer = _accountService.GetByUsernameWithInclude(User.Identity.Name);
            var newOrder = OrderMapper.MapViewToDtoModel(model);

            newOrder.Customer = orderCustomer;
            newOrder.ImageDto = new ImageDTO();
            if (model.ImageFile != null && model.ImageFile.ContentLength > (int)ByteFileSizes.Megabyte * 15)
            {
                ModelState.AddModelError("", @"The file size exceeds the maximum. (15 MB)");
                return View(model);
            }
            if(model.ImageFile != null)
            {
                var fileSplitName = model.ImageFile.FileName.Split('.');
                var fileExtension = (fileSplitName.Length > 1) ? "." + fileSplitName[1] : string.Empty;
                if (!ConfigurationsConstants.ImageFormats.Contains(fileExtension))
                {
                    ModelState.AddModelError("", @"Invalid image format");
                    return View(model);
                }

                var fileName = SaveImageOnServer(model.ImageFile);
                newOrder.ImageDto.ImagePath = "/Images/Uploads/Orders/" + fileName;
            }
            _orderService.Create(newOrder);
            return RedirectToAction("Index");
        }

        [HttpGet]
        [JwAuthorize]
        public ActionResult Edit(int id)
        {
            var order = _orderService.GetById(id);
            if (order.Customer.UserDto.Login != User.Identity.Name)
            {
                return RedirectToAction("AccessDenied", "Error");
            }

            if (order.OrderStateDto.Id != (int)OrderStates.New)
            {
                return RedirectToAction("AccessDenied", "Error");
            }

            var view = OrderMapper.MapDtoModelToView(order);
            return View(view);
        }

        [HttpPost]
        [JwAuthorize]
        [ValidateInput(false)]
        public ActionResult Edit(OrderViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            var updatedOrder = _orderService.GetById(viewModel.Id);

            if (updatedOrder.Customer.UserDto.Login != User.Identity.Name)
            {
                return RedirectToAction("AccessDenied", "Error");
            }

            if (updatedOrder.OrderStateDto.Id != (int)OrderStates.New)
            {
                return RedirectToAction("AccessDenied", "Error");
            }

            Mapper.Map(viewModel, updatedOrder);

            if (viewModel.ImageFile != null && viewModel.ImageFile.ContentLength > (int)ByteFileSizes.Megabyte * 15)
            {
                ModelState.AddModelError("", @"The file size exceeds the maximum. (15 MB)");
                return View(viewModel);
            }

            if (viewModel.ImageFile != null)
            {
                var fileSplitName = viewModel.ImageFile.FileName.Split('.');
                var fileExtension = (fileSplitName.Length > 1) ? "." + fileSplitName[1] : string.Empty;
                if (!ConfigurationsConstants.ImageFormats.Contains(fileExtension))
                {
                    ModelState.AddModelError("", @"Invalid image format");
                    return View(viewModel);
                }

                var fileName = SaveImageOnServer(viewModel.ImageFile);
                updatedOrder.ImageDto = new ImageDTO
                {
                    ImagePath = "/Images/Uploads/Orders/" + fileName
                };
            }

            _orderService.Update(updatedOrder);
            return RedirectToAction("Show", new { id = viewModel.Id });
        }

        [HttpGet]
        [JwAuthorize]
        public ActionResult Delete(int id)
        {
            var order = _orderService.GetById(id);
            if (order.Customer.UserDto.Login != User.Identity.Name)
            {
                return RedirectToAction("AccessDenied", "Error");
            }

            _orderService.Delete(order);
            return RedirectToAction("Index");
        }

        [HttpGet]
        [JwAuthorize]
        public async Task<ActionResult> ChoiceEmployer(int ordedId, int employId)
        {
            var order = _orderService.GetById(ordedId);
            if (order.Customer.UserDto.Login != User.Identity.Name)
            {
                return RedirectToAction("AccessDenied", "Error");
            }

            var newEmploy = _accountService.GetById(employId);
            order.Employer = newEmploy;
            order.OrderStateDto = new OrderStateDTO { Id = (int)OrderStates.InProgress };
            _orderService.Update(order);
            await NotifyAboutInviteChoice(order);
            return RedirectToAction("Show", new { id = ordedId });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SearchOrder(string search, FilterViewModel[] filters, int? page)
        {
            var orders = _orderSearchService.Search(search, page, filters);
            return PartialView("OrderPage/OrdersShow", orders);
        }

        [HttpGet]
        public ActionResult FinishOrder(int orderId)
        {
            var order = _orderService.GetById(orderId);
            if (!User.Identity.IsAuthenticated || order.Customer.UserDto.Login != User.Identity.Name)
            {
                return HttpNotFound("Finish order access denied");
            }
            var finishModel = new OrderFinishViewModel { Id = orderId };
            return PartialView("OrderPage/OrderFinishForm", finishModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> FinishOrder(OrderFinishViewModel orderModel)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("InternalServerError", "Error");
            }

            var order = _orderService.GetById(orderModel.Id);
            if (!User.Identity.IsAuthenticated || order.Customer.UserDto.Login != User.Identity.Name)
            {
                return HttpNotFound("Finish order access denied");
            }

            var ratingModel = new RatingDTO
            {
                AccountDto = order.Customer,
                OrderDto = order,
                Message = orderModel.Message,
                RatingValue = orderModel.RatingValue
            };

            if (order.RatingsDto.Any(rate => rate.AccountDto.UserDto.Login == User.Identity.Name))
            {
                return HttpNotFound("\"Add rating\" access denied");
            }

            order.OrderStateDto = new OrderStateDTO { Id = orderModel.OrderStateId };
            _orderService.Update(order);
            _ratingService.Create(ratingModel);
            await NotifyAboutStateChange(order);
            return RedirectToAction("Show", new { id = order.Id });
        }

        [HttpGet]
        public ActionResult AddRating(int orderId)
        {
            var order = _orderService.GetById(orderId);
            var currentUser = User.Identity.Name;
            if (!User.Identity.IsAuthenticated || order.Employer.UserDto.Login != currentUser)
            {
                return HttpNotFound("\"Add rating\" access denied");
            }
            var finishModel = new OrderFinishViewModel { Id = orderId };

            return PartialView("RatingsViews/AddRating", finishModel);
        }

        [HttpPost]
        [JwAuthorize]
        [ValidateInput(false)]
        public ActionResult AddRating(OrderFinishViewModel ratingViewModel)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("InternalServerError", "Error");
            }

            var order = _orderService.GetById(ratingViewModel.Id);
            var currentUser = User.Identity.Name;
            if (!User.Identity.IsAuthenticated || order.Employer.UserDto.Login != currentUser)
            {
                return HttpNotFound("\"Add rating\" access denied");
            }

            if (order.RatingsDto.Any(rate => rate.AccountDto.UserDto.Login == currentUser))
            {
                return RedirectToAction("Show", new { id = order.Id });
            }

            var ratingModel = new RatingDTO
            {
                AccountDto = order.Employer,
                OrderDto = order,
                Message = ratingViewModel.Message,
                RatingValue = ratingViewModel.RatingValue
            };
            _ratingService.Create(ratingModel);
            return RedirectToAction("Show", new { id = order.Id });
        }

        private async Task NotifyAboutStateChange(OrderDTO order)
        {
            var orderLink = Url.Action("Show", "Order", new { id = order.Id }, Request.Url.Scheme);
            if (!order.Employer.UserDto.ConfirmEmail) return;
            var body = ConfigurationsConstants.OrderStateChange + $"<a href=\"{orderLink}\">Order Link</a>";
            await _emailNotificator.SendNotificationAsync(order.Employer.UserDto.Email, body);
        }

        private async Task NotifyAboutInviteChoice(OrderDTO order)
        {
            var orderLink = Url.Action("Show", "Order", new { id = order.Id }, Request.Url.Scheme);
            if (!order.Employer.UserDto.ConfirmEmail) return;
            var body = ConfigurationsConstants.InviteChoiced + $"<a href=\"{orderLink}\">Order Link</a>";
            await _emailNotificator.SendNotificationAsync(order.Employer.UserDto.Email, body);
        }

        private string SaveImageOnServer(HttpPostedFileBase imageFile)
        {
            var name = Path.GetFileName(imageFile.FileName);
            if (name == null)
            {
                return null;
            }
            var fileSplitName = name.Split('.');
            var fileName = fileSplitName[0];
            var fileExtension = (fileSplitName.Length > 1) ? "." + fileSplitName[1] : string.Empty;
            if (!ConfigurationsConstants.ImageFormats.Contains(fileExtension))
            {
                throw new InvalidCastException("Image format is not supported.");
            }
            var hashName = Crypto.SHA256(fileName) + DateTime.Now.ToFileTime();

            fileName = hashName + fileExtension;
            var path = Path.Combine(Server.MapPath("~/Images/Uploads/Orders/"), fileName);
            imageFile.SaveAs(path);
            return fileName;

        }
    }
}