﻿using System;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;
using JustWork.Common.Infrastructure.Logging;
using JustWork.Logic.DTO;
using JustWork.Logic.Interface;
using JustWork.Model;
using JustWork.Model.UserModels;

namespace JustWork.Website.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IAccountService _accountService;
        private readonly ILogger _logger;

        public UserController(
            IUserService userService,
            IAccountService accountService,
            ILogger logger
            )
        {
            _userService = userService;
            _accountService = accountService;
            _logger = logger;
        }

        [Authorize]
        [HttpGet]
        public ActionResult EditEmail()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> EditEmail(EditEmailViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }
            var user = _userService.GetByLogin(User.Identity.Name);

            if (!Crypto.VerifyHashedPassword(user.PasswordHash, viewModel.Password))
            {
                ModelState.AddModelError("", @"Invalid password");
                return View(viewModel);
            }

            user.ConfirmEmail = false;
            user.Email = viewModel.Email.ToLower();
            _userService.Update(user);
            await SendEmailConfirmLetter(user.Login, user.Email);

            return RedirectToAction("ViewProfile", "Account", new { login = user.Login });
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Login(LoginUserViewModel loginModel, string ReturnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!ModelState.IsValid)
            {
                return View(loginModel);
            }
            var user = _userService.GetByLogin(loginModel.Name.ToLower());

            if (user == null)
            {
                var userCollection = _userService.GetByPredicate(usr => usr.Email == loginModel.Name.ToLower() && usr.ConfirmEmail).ToList();
                user = userCollection.Any() ? userCollection.First() : null;
            }

            if (user == null)
            {
                ModelState.AddModelError("", @"Not found user with this login or confirmed email adress");
                return View(loginModel);
            }

            if (!Crypto.VerifyHashedPassword(user.PasswordHash, loginModel.Password))
            {
                ModelState.AddModelError("", @"Invalid password");
                return View(loginModel);
            }

            FormsAuthentication.SetAuthCookie(user.Login, true);
            if (Url.IsLocalUrl(ReturnUrl))
            {
                return Redirect(ReturnUrl);
            }
            return RedirectToAction("ViewProfile", "Account", new { login = user.Login });
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Register()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel registerModel)
        {

            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!ModelState.IsValid)
            {
                return View(registerModel);
            }

            if (registerModel.Name.Contains(" ") || registerModel.Password.Contains(" "))
            {
                ModelState.AddModelError("", @"Fields must not contain spaces");
                return View(registerModel);
            }
            _logger.Information($"User: {registerModel.Name}, with email: {registerModel.Email}" +
                                "\nRegistered.");
            await SendEmailConfirmLetter(registerModel.Name, registerModel.Email);

            CreateAccount(registerModel);
            _logger.Information($"Set auth cookie for {registerModel.Name}");
            //FormsAuthentication.SetAuthCookie(registerModel.Name.ToLower(), true);
            Response.Cookies.Add(FormsAuthentication.GetAuthCookie(registerModel.Name.ToLower(), true));
            return RedirectToAction("ViewProfile", "Account", new { login = registerModel.Name.ToLower() });
        }

        private async Task SendEmailConfirmLetter(string login, string email)
        {
            using (var smtp = new SmtpClient())
            {
                _logger.Information("Create new SMPT instance.");
                var confirmEmailLink = Url.Action("ConfirmEmail", "User", new
                {
                    login,
                    email
                }, Request.Url.Scheme);

                var m = new MailMessage(
                    new MailAddress("justworktech2017@gmail.com", "Just Work Team"),
                    new MailAddress(email))
                {
                    Subject = "Email confirmation",
                    Body = $"Dear {login} <br> Thank you for your registration, please " +
                           "click on the below link to confirm your e-mail adress:<br> <a href=\"" +
                           $"{confirmEmailLink}\"> Confirm email.</a> <br> Best Regards, <br> Just Work Team.",
                    IsBodyHtml = true
                };
                _logger.Information($"Mail ready to send.");
                await smtp.SendMailAsync(m);
                _logger.Information($"Mail is sended.");
            }
        }

        public ActionResult ConfirmEmail(string login, string email)
        {
            var user = _userService.GetByLogin(login);

            if (user == null)
            {
                return HttpNotFound("Error when email confirming. User not found.");
            }

            if (user.Email != email)
            {
                return HttpNotFound("Error when email confirming. E-mails do not match.");
            }

            user.ConfirmEmail = true;

            _userService.Update(user);
            FormsAuthentication.SetAuthCookie(user.Login.ToLower(), true);
            return RedirectToAction("Index", "Home", new { ConfirmedEmail = user.Email });
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult> Confirm()
        {
            var user = _userService.GetByLogin(User.Identity.Name);

            if (user == null)
            {
                return HttpNotFound("Error when email confirming. User not found.");
            }

            if (user.ConfirmEmail)
            {
                return RedirectToAction("Index", "Home");
            }

            await SendEmailConfirmLetter(user.Login, user.Email);
            ViewBag.Email = user.Email;
            return View();
        }

        [Authorize]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public ActionResult ConfirmInformation()
        {
            return View();
        }

        [HttpGet]
        [ValidateInput(false)]
        public JsonResult CheckNameOnExistForLogin(string name)
        {
            var loginResult = CheckOnExistUserForLogin(name);
            var emailResult = CheckEmailExist(name);
            var result = loginResult || emailResult;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ValidateInput(false)]
        public JsonResult CheckEmailOnExist(string email)
        {
            return Json(!CheckEmailExist(email), JsonRequestBehavior.AllowGet);
        }

        private bool CheckEmailExist(string email)
        {
            return _userService.GetByPredicate(usr => usr.Email == email).Any();
        }

        [HttpGet]
        [ValidateInput(false)]
        public JsonResult CheckNameOnExist(string name)
        {
            var result = !CheckOnExistUserForLogin(name);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private bool CheckOnExistUserForLogin(string login)
        {
            return _userService.GetByLogin(login.ToLower()) != null;
        }

        private void CreateAccount(RegisterViewModel registerModel)
        {
            _accountService.Create(new AccountDTO
            {
                BirthDate = DateTime.Now,
                UserDto = new UserDTO
                {
                    CreationDate = DateTime.Now,
                    Login = registerModel.Name.ToLower(),
                    PasswordHash = Crypto.HashPassword(registerModel.Password),
                    RoleDto = new RoleDTO { Id = registerModel.RoleId },
                    Email = registerModel.Email.ToLower(),
                    ConfirmEmail = false
                },
            });
        }
    }
}