﻿using System.Web.Mvc;
using JustWork.Common.Infrastructure.Logging;
using JustWork.Model;
using JustWork.Website.Infastructure.Interfaces;

namespace JustWork.Website.Controllers
{
    public class WorkController : Controller
    {
        private readonly ILogger _logger;
        private readonly ISearchService<WorkersViewModel> _workersSearchService;

        public WorkController(
            ISearchService<WorkersViewModel> workersSearchService,
            ILogger logger
            )
        {
            _workersSearchService = workersSearchService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult Index(int? page)
        {
           var workers = _workersSearchService.Search(page);
            _logger.Information("Search complete.");
           return View(workers);
        }

        [HttpPost]
        public ActionResult SearchWorkers(int? page)
        {
            var workers = _workersSearchService.Search(page);
            return PartialView("WorkersListShow", workers);
        }
    }
}