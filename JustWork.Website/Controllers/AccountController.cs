﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using JustWork.Common;
using JustWork.Common.Exceptions;
using JustWork.Logic.DTO;
using JustWork.Logic.Interface;
using JustWork.Website.Infastructure.Interfaces;
using JustWork.Website.Infastructure.Mappers;

namespace JustWork.Website.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly IOrderSearchService _orderSearchService;
        private readonly IOrderService _orderService;

        public AccountController(
            IAccountService accountService,
            IOrderSearchService orderSearchService,
            IOrderService orderService
            )
        {
            _accountService = accountService;
            _orderSearchService = orderSearchService;
            _orderService = orderService;
        }

        [Authorize]
        [HttpGet]
        public ActionResult ViewProfile(int? id, string login)
        {
            AccountDTO accountBll;

            if (id.HasValue)
            {
                accountBll = _accountService.GetByIdWithInclude(id.Value);
            }
            else if (!string.IsNullOrEmpty(login))
            {
                accountBll = _accountService.GetByUsernameWithInclude(login);
            }
            else
            {
                return RedirectToAction("HttpNotFound", "Error");
            }

            var accViewModel = AccountMapper.MapToProfileViewModel(accountBll);

            accViewModel.NewOrdersCount = _orderService.GetItemsCount(order => order.OrderStateId == (int)OrderStates.New &&
                                                                     order.Customer.Id == accountBll.Id);

            accViewModel.InProgressOrdersCount = _orderService.GetItemsCount(order => order.OrderStateId == (int)OrderStates.InProgress &&
                                                                                      (order.EmployerId == accountBll.Id ||
                                                                                       order.Customer.Id == accountBll.Id));

            accViewModel.CompleteOrdersCount = _orderService.GetItemsCount(order => order.OrderStateId == (int)OrderStates.Complete &&
                                                                                      (order.EmployerId == accountBll.Id ||
                                                                                       order.Customer.Id == accountBll.Id));

            accViewModel.CancelOrderCount = _orderService.GetItemsCount(order => order.OrderStateId == (int)OrderStates.Canceled &&
                                                                                      (order.EmployerId == accountBll.Id ||
                                                                                       order.Customer.Id == accountBll.Id));


            return View(accViewModel);
        }

        #region OrderHistoryregion  
        [HttpGet]
        public ActionResult GetNewOrders(int userId, int? numberOfPart)
        {
            var orders = _orderSearchService.SearchWithInviteSort(order =>
                order.OrderState.Id == (int)OrderStates.New &&
                (order.CustomerId == userId || order.EmployerId == userId),
                numberOfPart);
            return PartialView("OrderHistory/NewOrdersHistory", orders);
        }

        [HttpGet]
        public ActionResult GetInProgressOrders(int userId, int? numberOfPart)
        {
            var page = numberOfPart ?? 1;
            var orders = _orderSearchService.Search(null, page, null, order =>
                order.OrderStateId == (int)OrderStates.InProgress &&
                (order.CustomerId == userId || order.EmployerId == userId));
            return PartialView("OrderHistory/InProgressOrdersHistory", orders);
        }

        [HttpGet]
        public ActionResult GetCompleteOrders(int userId, int? numberOfPart)
        {
            var page = numberOfPart ?? 1;
            var orders = _orderSearchService.Search(null, page, null, order =>
                order.OrderStateId == (int)OrderStates.Complete &&
                (order.CustomerId == userId || order.EmployerId == userId));
            return PartialView("OrderHistory/CompleteOrdersHistory", orders);
        }

        [HttpGet]
        public ActionResult GetCancelOrders(int userId, int? numberOfPart)
        {
            var page = numberOfPart ?? 1;
            var orders = _orderSearchService.Search(null, page, null, order =>
                order.OrderStateId == (int)OrderStates.Canceled &&
                (order.CustomerId == userId || order.EmployerId == userId));
            return PartialView("OrderHistory/CompleteOrdersHistory", orders);
        }
        #endregion

        #region functionsForEditProfile
        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ChangeAdditionalInfo(int accountId, string text)
        {
            var account = _accountService.GetByIdWithInclude(accountId);
            if (account == null) return new HttpStatusCodeResult(404);
            AccountValidate(account);
            account.AdditionalInfo = text;
            _accountService.Update(account);
            return new EmptyResult();
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeBirthdate(int accountId, DateTime date)
        {
            var account = _accountService.GetByIdWithInclude(accountId);
            if (account == null) return new HttpStatusCodeResult(404);
            AccountValidate(account);
            account.BirthDate = date;
            _accountService.Update(account);
            return new EmptyResult();
        }

        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult NameChange(int accountId, string name)
        {
            var account = _accountService.GetByIdWithInclude(accountId);
            if (account == null) return new HttpStatusCodeResult(404);
            AccountValidate(account);
            account.Name = name;
            _accountService.Update(account);
            return new EmptyResult();
        }

        [Authorize]
        [HttpPost]
        public string ChangeAccountImage()
        {
            var accountId = int.Parse(System.Web.HttpContext.Current.Request.Form["HelpSectionAccount"]);
            var imageFile = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
            var account = _accountService.GetByIdWithInclude(accountId);

            AccountValidate(account);
            if (imageFile == null || imageFile.ContentLength <= 0)
            {
                throw new NullReferenceException();
            }

            if (imageFile.ContentLength > (int)ByteFileSizes.Megabyte * 15)
            {
                throw new InvalidOperationException("The file size exceeds the maximum");
            }

            var fileName = SaveImageOnServer(imageFile);

            account.Avatar.ImagePath = "/Images/Uploads/" + fileName;
            _accountService.Update(account);
            return account.Avatar.ImagePath;
        }

        [Authorize]
        [HttpPost]
        public ActionResult DeleteImage(int accountId)
        {
            var account = _accountService.GetByIdWithInclude(accountId);
            if (account == null) return new HttpStatusCodeResult(404);
            AccountValidate(account);
            account.Avatar.ImagePath = null;
            _accountService.Update(account);
            return new EmptyResult();
        }
        #endregion

        private string SaveImageOnServer(HttpPostedFile imageFile)
        {
           
            var fileSplitName = Path.GetFileName(imageFile.FileName).Split('.');
            var fileName = fileSplitName[0];
            var fileExtension = (fileSplitName.Length > 1) ? "." + fileSplitName[1] : string.Empty;
            if (!ConfigurationsConstants.ImageFormats.Contains(fileExtension))
            {
                throw new InvalidCastException("Image format is not supported.");
            }
            var hashName = Crypto.SHA256(fileName) + DateTime.Now.ToFileTime();

            fileName = hashName + fileExtension;
            var path = Path.Combine(Server.MapPath("~/Images/Uploads/"),
                fileName);
            imageFile.SaveAs(path);
            return fileName;
        }

        private void AccountValidate(AccountDTO account)
        {
            if (account == null)
            {
                throw new ItemNotFoundException("Account not found");
            }
            if (account.UserDto.Login != User.Identity.Name)
            {
                throw new MemberAccessException("Access denied");
            }
        }
    }
}