﻿$(document).ready(function () {
    $(".avatar").on("error", function () {
        onImageErrorHandler(this);
    });

    $(".service-offers__image").on("error", function () {
        handleOrderErrorImage(this);
    });


    $(".order-avatar-image-show").on("click", function () {
        $(".enlargeImageModalSource").attr("src", $(this).attr("src"));
        $("#enlargeImageModal").modal("show");
    });
});

function getParameterByName(name) {
    const match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, " "));
}

function handleOrderErrorImage(obj) {
    $(obj).attr("src", "/Images/orderError.jpg");
}

$(document).ready(function() {
    $(document).on("change", ".btn-file :file", function () {
        const input = $(this);
        const label = input.val().replace(/\\/g, "/").replace(/.*\//, "");
        input.trigger("fileselect", [label]);
    });

    $(".btn-file :file").on("fileselect", function (event, label) {
        const input = $(this).parents(".input-group").find(":text");
        const log = label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });

    $("#ImageFile").change(function () {
        readUrl(this);
    });
});

function readUrl(input) {
    if (input.files && input.files[0]) {
        const reader = new FileReader();
        reader.onload = function (e) {
            $(".order-image").attr("src", e.target.result);
            $(".order-image").css('visibility', 'visible');
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function onImageErrorHandler(image) {
    $(image).attr("src", "/Images/avatarError.png");
}

$("#searchContainerBtn").click(function () {
    const searchVal = $("#searchContainerInput").val();
    let searchQuery = "";
    if (searchVal) {
        searchQuery = "?search=" + searchVal;
    }
    const select = $("#duration").find("option:selected");
    const maxVal = select.attr("data-endRange");
    const minVal = select.attr("data-stRange");
    const criterion = select.attr("data-criterion");
    var criterionQuery = "";
    if (criterion) {
        if (searchVal) {
            criterionQuery = "&criterion=" + criterion + "&minValue=" + minVal + "&maxValue=" + maxVal;
        } else {
            criterionQuery = "?criterion=" + criterion + "&minValue=" + minVal + "&maxValue=" + maxVal;
        }
    }
    $(location).attr('href', '/Order/' + searchQuery + criterionQuery);
});