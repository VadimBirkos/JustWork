﻿var executeTimeout = function (callBack, timeout) {
    if (callBack.selectTimeOut) {
        clearTimeout(callBack.selectTimeOut);
    }
    callBack.selectTimeOut = setTimeout(function () {
        callBack();
    }, timeout);
}

$(document).ready(function () {
    const search = getParameterByName("search");
    const maxDateVal = getParameterByName("maxValue");
    if (maxDateVal) {
        $('.filterCheckbox[data-endrange=' + maxDateVal + ']').attr("checked", "true");
    }

    if (search) {
        $("#searchInput").val(search);
    }
});

$(document).on("click", ".commonSortBtn", function () {
    const page = $(this).attr("data-pagesize");
    var pageLoad = function() {
        loadNewOrderListContent(page);
    }
    executeTimeout(pageLoad, 300);
});

$("#searchInput").keyup(function () {
    executeTimeout(loadNewOrderListContent, 500);
});

function prepareFilters() {
    const checkBoxes = $(".filterCheckbox");
    var filterModel = { params: [] };
    jQuery.each(checkBoxes, function (i, val) {
        if (val.checked) {
            const model = {
                "MinValue": $(val).attr("data-stRange"),
                "MaxValue": $(val).attr("data-endRange"),
                "Criterion": $(val).attr("data-criterion")
            };
            filterModel.params.push(model);
        }
    });
    return filterModel;
}

function loadNewOrderListContent(page) {
    const listSelector = $(".orderList");
    const filters = prepareFilters();
    const searchText = $("#searchInput").val();
    var sortBtnVal;
    if (!page) {
        sortBtnVal = $(".selected.sortBtn").attr("data-pagesize");
    } else {
        sortBtnVal = page;
    }
    $.ajax({
        type: "POST",
        url: "/Order/SearchOrder",
        data: {
            search: searchText,
            filters: filters.params,
            page: sortBtnVal
        },
        success: function (data) {
            listSelector.html(data);
        },
        error: function () {
            alert("Error when the request is processed");
        }
    });
}

$(document).on("click",".search-button-submit", function() {
    executeTimeout(loadNewOrderListContent, 300);
});

$("#searchInput").keydown(function (e) {
    if (e.keyCode === 13) {
        executeTimeout(loadNewOrderListContent, 300);
    }
});

$(".filterCheckbox").change(function () {
    executeTimeout(loadNewOrderListContent, 300);
});
