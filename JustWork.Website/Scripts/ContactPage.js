﻿var map;

var defult = new google.maps.LatLng(53.9117408, 27.6336841, 18.75);
var mapCoordinates = new google.maps.LatLng(53.9117408, 27.6336841, 18.75);

var markers = [];
var image = new google.maps.MarkerImage(
    '/Content/img/map-marker.png',
    new google.maps.Size(84, 70),
    new google.maps.Point(0, 0),
    new google.maps.Point(60, 60)
);

function addMarker() {
    markers.push(new google.maps.Marker({
            position: defult,
            raiseOnDrag: false,
            icon: image,
            map: map,
            draggable: false
        }
    ));
}

function initialize() {
    var mapOptions = {
            backgroundColor: "#ffffff",
            zoom: 14,
            disableDefaultUI: true,
            center: mapCoordinates,
            zoomControl: true,
            scaleControl: true,
            scrollwheel: true,
            disableDoubleClickZoom: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP

        }
        ;
    map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
    addMarker();
}
google.maps.event.addDomListener(window, 'load', initialize);