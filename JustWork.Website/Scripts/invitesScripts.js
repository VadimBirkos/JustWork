﻿$(".allInvites").on("click", ".invite-delete", function () {
    const inviteId = $(this).attr("val");
    deleteInviteQuery(inviteId);
});

$(".newInvite").on("submit", function (e) {
    const myForm = $("#newInviteForm");
    if (myForm.valid()) {
        sendNewInviteQuery();
    }
    e.preventDefault();
});

function sendNewInviteQuery() {
    const url = "/Invite/Add";
    const formData = $("#newInviteForm").serialize();
    $.ajax({
        type: "POST",
        url: url,
        data: formData,
        success: function (data) {
            showNewInvite(data);
        },
        error: function () {
            alert("Error when invite add.");
        }
    });
}

function showNewInvite(data) {
    $("#newInvite").empty();
    const url = "/Invite/InviteShow/?id=" + data["Id"];
    $.ajax({
        url: url,
        success: function (data) {
            $(".allInvites").append(data);
        }
    });
}

function deleteInviteQuery(id) {
    var result = confirm("Want to delete?");
    if (result) {
    const url = "/Invite/Delete";
    $.ajax({
        type: "DELETE",
        url: url,
        data: { inviteId: id },
        success: function () {
            clearInviteBlock();
        },
        error: function () {
            alert("Error when invite delete.");
        }
    });
    }
}

function clearInviteBlock() {
    $("#newInvite").show();

    $("#newInvite").load("/Invite/LoadlIventForm",
        function () {
            const id = $("#Id").val();
            $("#OrderId").val(id);
            const cost = $("#Budget").val();
            $("#Cost").val(cost);
        });

    const inviteDelBtn = $(".invite-delete");
    inviteDelBtn.closest('div[class ="invite-block"]').empty();

}