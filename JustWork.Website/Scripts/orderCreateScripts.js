﻿const dateFormat = "mm.dd.yy";

$(function () {
    var from;
    var to = $("#EndDate").datepicker({
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 2,
        showOtherMonths: true,
        selectOtherMonths: true,
        defaultDate: "+1",
        dateFormat: dateFormat,
        minDate: "+0"
    })
        .on("change", function () {
            from.datepicker("option", "maxDate", getDate($(this)));
        });

    from = $("#StartDate")
        .datepicker({
            changeMonth: true,
            changeYear: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            minDate: "+0",
            defaultDate: "+1",
            numberOfMonths: 2,
            dateFormat: dateFormat
        })
        .on("change", function () {
            to.datepicker("option", "minDate", getDate($(this)));
        });
});

function getDate(element) {
    var date;
    try {
        const elValue = element.val();
        date = $.datepicker.parseDate(dateFormat, elValue);
    } catch (error) {
        date = null;
    }
    return date;
};

jQuery.validator.addMethod("date",
    function (value, element) {
        var ok = true;
        try {
            $.datepicker.parseDate(dateFormat, value);
        }
        catch (err) {
            ok = false;
        }
        return ok;
    });