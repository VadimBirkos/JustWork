﻿$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $(document).on("change", ".btn-file :file", function () {
        const input = $(this);
        const label = input.val().replace(/\\/g, "/").replace(/.*\//, "");
        input.trigger("fileselect", [label]);
    });

    $(".btn-file :file").on("fileselect", function (event, label) {
        const input = $(this).parents(".input-group").find(":text");
        const log = label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });

    $("#imageFileChoice").change(function () {
        readUrl(this);
    });

    $.fn.editable.defaults.mode = "inline";

    $("#nameField").editable({
        type: "text",
        url: function (params) {
            return tryNameChange(params);
        },
        validate: function (value) {
            return validateName(value);
        },
        title: "Enter username"
    });

    $("#dateOfBirth").editable({
        type: "date",
        format: "mm/dd/yyyy",
        viewformat: "mm.dd.yyyy",
        datepicker: datepickerConfig,
        url: function (params) {
            tryChangeDateofBirth(params);
        },
        title: "Choice date",
        inputclass: 'date-editable-class'
    });

    $("#aboutMyself").editable({
        url: function (params) {
            tryChangeAdditionalInfo(params);
        },
        validate: function (value) {
            return validateAdditionalInfo(value);
        },
        title: "Write about yourself"
    });
});

$(function () {
    $(".avatar").on("click", function () {
        $(".enlargeImageModalSource").attr("src", $(this).attr("src"));
        $("#enlargeImageModal").modal("show");
    });
    const usrId = $("#cancelOrdersAccordion").attr("data-usrId");
    const query = $.get("/Account/GetNewOrders/?userId=" + usrId, function (data) {
        $(".newOrdersBody").html(data);
    });
    query.fail(function () {
        alert("Error when orders load");
    });
});

$("#cancelOrdersAccordion").on("show.bs.collapse", function () {
    const usrId = $(this).attr("data-usrId");
    $.get("/Account/GetCancelOrders/?userId=" + usrId, function (data) {
        $(".cancelOrdersBody").html(data);
    });
});

$(document).on("click", ".loadMoreCancelOrderHistory", function () {
    const accId = $("#AccountId").val();
    const page = $(this).attr("data-currPage");
    $(this).hide();
    const query = $.get("/Account/GetCancelOrders/?userId=" + accId + "&numberOfPart=" + page, function (data) {
        const resultHtml = $(".cancelOrdersBody").html();
        $(".cancelOrdersBody").html(resultHtml + data);
    });
    query.fail(function () {
        alert("Error when orders load");
    });
});

$("#completeOrdersAccordion").on("show.bs.collapse", function () {
    const usrId = $(this).attr("data-usrId");
    $.get("/Account/GetCompleteOrders/?userId=" + usrId, function (data) {
        $(".completeOrdersBody").html(data);
    });
});

$(document).on("click", ".loadMoreCompleteOrderHistory", function () {
    const accId = $("#AccountId").val();
    const page = $(this).attr("data-currPage");
    $(this).hide();
    const query = $.get("/Account/GetCompleteOrders/?userId=" + accId + "&numberOfPart=" + page, function (data) {
        const resultHtml = $(".completeOrdersBody").html();
        $(".completeOrdersBody").html(resultHtml + data);
    });
    query.fail(function () {
        alert("Error when orders load");
    });
});

$("#inProgressOrdersAccordion").on("show.bs.collapse", function () {
    const usrId = $(this).attr("data-usrId");
    const query = $.get("/Account/GetInProgressOrders/?userId=" + usrId, function (data) {
        $(".inProgressOrdersBody").html(data);
    });
    query.fail(function () {
        alert("Error when orders load");
    });
});

$(document).on("click", ".loadMoreInProgressOrderHistory", function () {
    const accId = $("#AccountId").val();
    const page = $(this).attr("data-currPage");
    $(this).hide();
    const query = $.get("/Account/GetInProgressOrders/?userId=" + accId + "&numberOfPart=" + page, function (data) {
        const resultHtml = $(".inProgressOrdersBody").html();
        $(".inProgressOrdersBody").html(resultHtml + data);
    });
    query.fail(function () {
        alert("Error when orders load");
    });
});

$("#newOrdersAccordion").on("show.bs.collapse", function () {
    const usrId = $(this).attr("data-usrId");
    const query = $.get("/Account/GetNewOrders/?userId=" + usrId, function (data) {
        $(".newOrdersBody").html(data);
    });
    query.fail(function () {
        alert("Error when orders load");
    });
});

$(document).on("click", ".loadMoreNewOrderHistory", function () {
    const accId = $("#AccountId").val();
    const page = $(this).attr("data-currPage");
    $(this).hide();
    const query = $.get("/Account/GetNewOrders/?userId=" + accId + "&numberOfPart=" + page, function (data) {
        const resultHtml = $(".newOrdersBody").html();
        $(".newOrdersBody").html(resultHtml + data);
    });
    query.fail(function () {
        alert("Error when orders load");
    });
});

$(document).on("click", "#dateOfBirth", function () {
    $(".hasDatepicker").attr("readonly", "true");
});

$(document).on("click", "#cancelSaveBtn", function() {
    $("#imageSaveBlock").hide();
    if (window.defaultImage) {
        $(".avatar").attr("src", window.defaultImage);
    }
});

$("#newImage").click(function () {
    window.defaultImage = $(".avatar").attr("src");
    displayImageChangeBlock();
});

$("#imageDelteBtn").click(function () {
    deleteImage();
});

$("#saveImageBtn").click(function () {
    saveImageBtn();
});

function readUrl(input) {
    if (input.files && input.files[0]) {
        const reader = new FileReader();
        reader.onload = function (e) {
            $("#avatar").attr("src", e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function deleteImage() {

    var result = confirm("Do you want to delete your avatar?");
    if (result) {
        const accountId = $("#AccountId").val();

        $.ajax({
            url: "/Account/DeleteImage",
            type: "POST",
            data: { accountId: accountId },
            success: function() {
                onSuccessImageSave("/Images/guest.png");
                $("#imageDelteBtn").hide();
                $("#imageSaveBlock").hide();
            },
            error: function() {
                alert("Server error when image uploading, try later.");
            }
        });
    }
}

function displayImageChangeBlock() {
    $("#imageSaveBlock").show();
}

function saveImageBtn() {
    const data = new FormData();
    const images = $("#imageFileChoice")[0].files;
    if (images.length > 0) {
        data.append("HelpSectionImages", images[0]);
    } else {
        alert("Error when picture changing, try later.");
        return;
    }
    const accountId = $("#AccountId").val();
    data.append("HelpSectionAccount", accountId);
    $.ajax({
        url: "/Account/ChangeAccountImage",
        type: "POST",
        processData: false,
        contentType: false,
        data: data,
        success: function (response) {
            onSuccessImageSave(response);
        },
        error: function () {
            alert("Error when picture changing, try later.");
        }
    });
}

function onSuccessImageSave(path) {
    $("#avatar").attr("src", path);
    $("#imageSaveBlock").hide();
}

function tryChangeDateofBirth(params) {
    var d = new $.Deferred();
    $.ajax({
        type: "POST",
        url: "/Account/ChangeBirthdate",
        data: {
            accountId: params.pk,
            date: params.value
        },
        success: function () {
            d.resolve();
        },
        error: function () {
            alert("Error when changing the date of birth");
        }
    });
    return d.promise();
}

function validateAdditionalInfo(value) {
    if ($.trim(value) === "") {
        return "This field is required";
    }

    if (value.length > 1100) {
        return "This field should be contain less than 1100 symbols";
    }
}

function tryChangeAdditionalInfo(params) {
    var d = new $.Deferred();
    $.ajax({
        type: "POST",
        url: "/Account/ChangeAdditionalInfo",
        data: {
            accountId: params.pk,
            text: params.value
        },
        success: function () {
            d.resolve();
        },
        error: function () {
            alert("Error when changing the additional info");
            d.reject();
        }
    });
    return d.promise();
}

function validateName(value) {
    if ($.trim(value) === "") {
        return "This field is required";
    }
    if (value.length > 15) {
        return "This field should be contain less than 15 symbols";
    }
}

function tryNameChange(params) {
    var d = new $.Deferred();
    $.ajax({
        type: "POST",
        url: "/Account/NameChange",
        data: {
            accountId: params.pk,
            name: params.value
        },
        success: function () {
            d.resolve();
        },
        error: function () {
            alert("Error when changing the name");
        }
    });
    return d.promise();
}

function logger(selectedDate) {
    if (console && console.log) console.log(selectedDate);
}

function getDate(element) {
    const dateFormat = "mm.dd.yy";
    var date;
    try {
        const elValue = element.text();
        date = $.datepicker.parseDate(dateFormat, elValue);
    } catch (error) {
        date = null;
    }
    return date;
};

const datepickerConfig = {
    changeMonth: true,
    changeYear: true,
    dateFormat: "mm.dd.yyyy",
    showOtherMonths: true,
    selectOtherMonths: true,
    numberOfMonths: 2,
    default: getDate($("#dateOfBirth")),
    minDate: "-100Y",
    maxDate: "-18Y",
    showButtonPanel: true,
    yearRange: "-100:-10"
}