﻿using System.Collections.Generic;
using JustWork.Common;

namespace JustWork.Logic.DTO
{
    public class PaidTypeDTO:IEntity
    {
        public int Id { get; set; }
        public string Type { get; set; }

        public ICollection<UserPaidDTO> UserPaidsDto { get; set; }

        public PaidTypeDTO()
        {
            UserPaidsDto = new HashSet<UserPaidDTO>();
        }
    }
}