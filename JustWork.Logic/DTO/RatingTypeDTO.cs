﻿using System.Collections.Generic;
using JustWork.Common;

namespace JustWork.Logic.DTO
{
    public class RatingTypeDTO:IEntity
    {
        public int Id { get; set; }
        public string Type { get; set; }

        public ICollection<RatingDTO> RatingsDto { get; set; }

        public RatingTypeDTO()
        {
            RatingsDto = new HashSet<RatingDTO>();
        }
    }
}