﻿using System.Collections.Generic;
using JustWork.Common;

namespace JustWork.Logic.DTO
{
    public class ImageDTO : IEntity
    {
        public int Id { get; set; }
        public string ImagePath { get; set; }

        public ICollection<PortfolioJobDTO> PortfolioJobsDto { get; set; }
        public ICollection<AccountDTO> AccountsDto { get; set; }
        public ICollection<OrderDTO> OrdersDto { get; set; }    

        public ImageDTO()
        {
            OrdersDto = new HashSet<OrderDTO>();
            PortfolioJobsDto = new HashSet<PortfolioJobDTO>();
            AccountsDto = new HashSet<AccountDTO>();
        }
    }
}