﻿using System;
using System.Collections.Generic;
using JustWork.Common;

namespace JustWork.Logic.DTO
{
    public sealed class OrderDTO : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public double? Budget { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public OrderStateDTO OrderStateDto { get; set; }
        public AccountDTO Customer { get; set; }
        public AccountDTO Employer { get; set; }

        public ImageDTO ImageDto { get; set; }

        public ICollection<RatingDTO> RatingsDto { get; set; }
        public ICollection<ServiceTypeDTO> ServiceTypesDto { get; set; }
        public ICollection<OrderInviteDTO> OrderInvites { get; set; }

        public OrderDTO()
        {
            ImageDto = new ImageDTO();
            ServiceTypesDto = new HashSet<ServiceTypeDTO>();
            RatingsDto = new HashSet<RatingDTO>();
        }
    }
}