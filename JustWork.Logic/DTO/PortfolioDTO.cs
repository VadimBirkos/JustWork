﻿using System.Collections.Generic;
using JustWork.Common;

namespace JustWork.Logic.DTO
{
    public class PortfolioDTO:IEntity
    {
        public int Id { get; set; }
        public string Description { get; set; }

        public AccountDTO AccountDto { get; set; }

        public ICollection<PortfolioJobDTO> PortfolioJobsDto { get; set; }

        public PortfolioDTO()
        {
            PortfolioJobsDto = new HashSet<PortfolioJobDTO>();
        }
    }
}