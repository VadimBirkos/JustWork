﻿using System;
using System.Collections.Generic;
using JustWork.Common;

namespace JustWork.Logic.DTO
{
    public sealed class AccountDTO:IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AdditionalInfo { get; set; }
        public DateTime BirthDate { get; set; }

        public UserDTO UserDto { get; set; }
        public ImageDTO Avatar { get; set; }
        public PortfolioDTO PortfolioDto { get; set; }

        public ICollection<ServiceTypeDTO> ServiceTypesDto { get; set; }    
        public ICollection<RatingDTO> RatingsDto { get; set; }
        public ICollection<UserPaidDTO> UserPaidsDto { get; set; }
        public ICollection<OrderInviteDTO> OrderInvites { get; set; }

        public AccountDTO()
        {
            Avatar = new ImageDTO();
            ServiceTypesDto = new HashSet<ServiceTypeDTO>();
            RatingsDto = new HashSet<RatingDTO>();
            UserPaidsDto = new HashSet<UserPaidDTO>();
            OrderInvites = new HashSet<OrderInviteDTO>();
        }
    }
}