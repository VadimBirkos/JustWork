﻿using System.Collections.Generic;
using JustWork.Common;

namespace JustWork.Logic.DTO
{
    public class PortfolioJobDTO : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int TeamSize { get; set; }

        public PortfolioDTO PortfolioDto { get; set; }

        public ICollection<ImageDTO> ImagesDto { get; set; }

        public PortfolioJobDTO()
        {
            ImagesDto = new HashSet<ImageDTO>();
        }
    }
}