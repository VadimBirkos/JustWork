﻿using System.Collections.Generic;
using JustWork.Common;

namespace JustWork.Logic.DTO
{
    public class OrderStateDTO:IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<OrderDTO> OrdersDto{ get; set; }

        public OrderStateDTO()
        {
            OrdersDto = new HashSet<OrderDTO>();
        }
    }
}