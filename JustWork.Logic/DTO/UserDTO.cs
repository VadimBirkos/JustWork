﻿using System;
using JustWork.Common;

namespace JustWork.Logic.DTO
{
    public class UserDTO:IEntity
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string PasswordHash { get; set; }
        public DateTime CreationDate { get; set; }
        public string Email { get; set; }
        public bool ConfirmEmail { get; set; }

        public AccountDTO AccountDto { get; set; }
        public RoleDTO RoleDto { get; set; }
    }
}