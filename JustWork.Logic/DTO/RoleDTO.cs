﻿using System.Collections.Generic;
using JustWork.Common;

namespace JustWork.Logic.DTO
{
    public class RoleDTO:IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public IEnumerable<UserDTO> UsersDto { get; set; }

        public RoleDTO()
        {
            UsersDto = new HashSet<UserDTO>();
        }
    }
}