﻿using JustWork.Common;

namespace JustWork.Logic.DTO
{
    public class RatingDTO:IEntity
    {
        public int Id { get; set; }
        public double RatingValue { get; set; }
        public string Message { get; set; }

        //public RatingTypeDTO RatingTypeDto { get; set; }
        public AccountDTO AccountDto { get; set; }
        public OrderDTO OrderDto { get; set; }
    }
}