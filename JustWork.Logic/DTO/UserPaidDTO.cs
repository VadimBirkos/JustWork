﻿using JustWork.Common;

namespace JustWork.Logic.DTO
{
    public class UserPaidDTO:IEntity
    {
        public int Id { get; set; }
        public double CostPerType { get; set; }

        public PaidTypeDTO PaidTypeDto { get; set; }
        public AccountDTO AccountDto { get; set; }
    }
}