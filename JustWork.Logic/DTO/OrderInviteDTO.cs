﻿using JustWork.Common;

namespace JustWork.Logic.DTO
{
    public class OrderInviteDTO:IEntity
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public double Cost { get; set; }
        public AccountDTO Employer { get; set; }
        public OrderDTO Order { get; set; }
    }
}