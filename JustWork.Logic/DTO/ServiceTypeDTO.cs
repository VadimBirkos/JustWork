﻿using System.Collections.Generic;
using JustWork.Common;

namespace JustWork.Logic.DTO
{
    public class ServiceTypeDTO:IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<AccountDTO> AccountsDto { get; set; }
        public ICollection<OrderDTO> OrdersDto{ get; set; }

        public ServiceTypeDTO()
        {
            OrdersDto = new HashSet<OrderDTO>();
            AccountsDto = new HashSet<AccountDTO>();
        }
    }
}