﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using JustWork.Common.Exceptions;
using JustWork.DataProvider.DalModels;
using JustWork.DataProvider.Interface;
using JustWork.Logic.DTO;
using JustWork.Logic.Interface;

namespace JustWork.Logic.Implementation
{
    public class UserService : BaseService<User, UserDTO, IRepository<User>>, IUserService
    {
        private readonly IService<RoleDTO> _roleService;

        public UserService(
            IUnitOfWork unitOfWork,
            IRepository<User> repository,
            IService<RoleDTO> roleService
            ) : base(unitOfWork, repository)
        {
            _roleService = roleService;
        }

        public UserDTO GetByLogin(string login)
        {
            var users = Repository.FindByPredicate(usr => usr.Login == login).ToList();
            return !users.Any() ? null : MapToDtoEntity(users.First());
        }

        public IEnumerable<UserDTO> GetByRole(string role)
        {
            var users = Repository.FindByPredicate(rol => rol.Role.Title == role);
            return MapToDtoEntityList(users);
        }

        public IEnumerable<UserDTO> GetByPredicate(Expression<Func<User, bool>> predicate)
        {
            var dalUsers = Repository.FindByPredicate(predicate).ToList();
            return MapToDtoEntityList(dalUsers);
        }

        public override void Create(UserDTO entity)
        {
            entity.RoleDto = _roleService.GetById(entity.RoleDto.Id);
            var dalEntity = MapToDalEntity(entity);
            dalEntity.RoleId = entity.RoleDto.Id;
            Repository.Create(dalEntity);
            UnitOfWork.Commit();
        }

        public override void Update(UserDTO entity)
        {
            var dalUser = Repository.FindByIdWithTrack(entity.Id);
            var dalEntity = Mapper.Map(entity, dalUser);
            Repository.Update(dalEntity);
            UnitOfWork.Commit();
        }

        private User GetDalUser(int id)
        {
            var dalUser = Repository.FindById(id);
            if (dalUser == null)
            {
                throw new ItemNotFoundException();
            }
            return dalUser;
        }
    }
}