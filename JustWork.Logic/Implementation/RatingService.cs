﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using JustWork.DataProvider.DalModels;
using JustWork.DataProvider.Interface;
using JustWork.Logic.AutoMapperProfiles;
using JustWork.Logic.DTO;
using JustWork.Logic.Interface;

namespace JustWork.Logic.Implementation
{
    public class RatingService:BaseService<Rating, RatingDTO, IRepository<Rating>>, IRatingService
    {
        private readonly IService<ImageDTO> _imageService;

        public RatingService(
            IUnitOfWork unitOfWork, 
            IRepository<Rating> repository,
            IService<ImageDTO> imageService
            ) : base(unitOfWork, repository)
        {
            _imageService = imageService;
        }

        public IEnumerable<RatingDTO> GetRatingByOrder(int orderId)
        {
            var dalRatings = Repository.FindByPredicate(rate => rate.OrderId == orderId).ToList();
            var bllRatings = MapToDtoEntityList(dalRatings);
            return bllRatings;
        }

        public IEnumerable<RatingDTO> GetByPredicate(Expression<Func<Rating, bool>> predicate)
        {
            var dalEntitties = Repository.FindByPredicate(predicate).OrderByDescending(order => order.Id).Take(5).ToList();
            var dtoEntities = MapToDtoEntityList(dalEntitties).ToList();
            foreach (var dtoEntity in dtoEntities)
            {
                dtoEntity.AccountDto.Avatar = _imageService.GetById(dtoEntity.AccountDto.Avatar.Id);
            }
            return dtoEntities;
        }

        public override void Create(RatingDTO entity)
        {
            var dalEntity = MapToDalEntity(entity);
            Repository.Create(dalEntity);
            UnitOfWork.Commit();
        }

        protected override Rating MapToDalEntity(RatingDTO dtoEntity)
        {
            return Mapper.Map<RatingDTO, Rating>(dtoEntity);
        }

        protected override IEnumerable<RatingDTO> MapToDtoEntityList(IEnumerable<Rating> dalList)
        {
            return Mapper.Map<IEnumerable<Rating>, IEnumerable<RatingDTO>>(dalList);
        }

        protected override RatingDTO MapToDtoEntity(Rating dalEntity)
        {
            return Mapper.Map<Rating, RatingDTO>(dalEntity);
        }
    }
}