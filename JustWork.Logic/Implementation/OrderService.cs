﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using AutoMapper;
using JustWork.Common;
using JustWork.Common.Infrastructure.Logging;
using JustWork.DataProvider.DalModels;
using JustWork.DataProvider.Interface;
using JustWork.Logic.DTO;
using JustWork.Logic.Interface;

namespace JustWork.Logic.Implementation
{
    public class OrderService : BaseService<Order, OrderDTO, IRepository<Order>>, IOrderService
    {
        private readonly ILogger _logger;
        private readonly Func<Order, int> _orderById = order => order.Id;
        private readonly Func<Order, int> _orderByInvite = order => order.OrderInvites.Count;

        public OrderService(
            IUnitOfWork unitOfWork,
            IRepository<Order> repository,
            ILogger logger
            ) : base(unitOfWork, repository)
        {
            _logger = logger;
        }

        public override OrderDTO GetById(int key)
        {
            var dalEntity = Repository.FindByIdWithTrack(key);
            var dtoEntity = MapToDtoEntity(dalEntity);
            return dtoEntity;
        }

        public override void Update(OrderDTO entity)
        {
            var dalOrder = Repository.FindByIdWithTrack(entity.Id);
            Mapper.Map(entity, dalOrder);
            Repository.Update(dalOrder);
            UnitOfWork.Commit();
        }

        public override IEnumerable<OrderDTO> GetAll()
        {
            var dalEntities = Repository.GetAll().ToList();
            return MapToDtoEntityList(dalEntities);
        }

        public override IEnumerable<OrderDTO> GetByPredicate(Expression<Func<OrderDTO, bool>> predicate)
        {
            var predicateCompile = predicate.Compile();
            var dalEntities = Repository.GetAll().ToList();
            var bllEntities = MapToDtoEntityList(dalEntities).ToList();
            var result = bllEntities.Where(predicateCompile);
            return result;
        }

        public Tuple<IEnumerable<OrderDTO>, int> GetByPredicate(Expression<Func<Order, bool>> predicate, int page)
        {
            var totalItemCount = Repository.FindByPredicate(predicate).Count();
            const int pageSize = ConfigurationsConstants.ItemCountOnPage;
            var skipCount = (page - 1) * pageSize;
            var dalList = Repository.FindByPredicate(predicate);
            
            var resultDalList = dalList.OrderByDescending(_orderById).Skip(skipCount).Take(pageSize).ToList();
            var bllList = MapToDtoEntityList(resultDalList).ToList();
            var result = new Tuple<IEnumerable<OrderDTO>, int>(bllList, totalItemCount);

            return result;
        }

        public Tuple<IEnumerable<OrderDTO>, int> SortByInvites(Expression<Func<Order, bool>> predicate, int? page)
        {
            var currentPage = page ?? 1;
            var totalItemCount = Repository.FindByPredicate(predicate).Count();
            const int pageSize = ConfigurationsConstants.ItemCountOnPage;
            var skipCount = (currentPage - 1) * pageSize;

            var dalList = Repository.FindByPredicate(predicate);
            var resulrDalList = dalList.OrderByDescending(_orderById).ThenByDescending(_orderByInvite).Skip(skipCount).Take(pageSize).ToList();
            var bllList = MapToDtoEntityList(resulrDalList).ToList();
            var result = new Tuple<IEnumerable<OrderDTO>, int>(bllList, totalItemCount);
            return result;
        }

        public int GetItemsCount(Expression<Func<Order, bool>> predicate)
        {
            return Repository.FindByPredicate(predicate).Count();
        }

        public override void Create(OrderDTO entity)
        {
            entity.OrderStateDto = new OrderStateDTO { Id = (int)OrderStates.New };
            var dalEntity = MapToDalEntity(entity);
          Repository.Create(dalEntity);
            UnitOfWork.Commit();
        }

        private void ShowInfoInLogger(object obj)
        {
            foreach (var propertyInfo in obj.GetType()
                .GetProperties(
                    BindingFlags.Public
                    | BindingFlags.Instance))
            {
                _logger.Information(propertyInfo.Name + ":    " + propertyInfo.GetValue(obj));
            }
        }

        protected override OrderDTO MapToDtoEntity(Order dalEntity)
        {
            return Mapper.Map<Order, OrderDTO>(dalEntity);
        }

        protected override Order MapToDalEntity(OrderDTO dtoEntity)
        {
            return Mapper.Map<OrderDTO, Order>(dtoEntity);
        }

        protected override IEnumerable<OrderDTO> MapToDtoEntityList(IEnumerable<Order> dalList)
        {
            return Mapper.Map<IEnumerable<Order>, IEnumerable<OrderDTO>>(dalList);
        }
    }
}