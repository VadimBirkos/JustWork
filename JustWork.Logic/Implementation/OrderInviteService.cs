﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using JustWork.DataProvider.DalModels;
using JustWork.DataProvider.Interface;
using JustWork.Logic.AutoMapperProfiles;
using JustWork.Logic.DTO;
using JustWork.Logic.Interface;

namespace JustWork.Logic.Implementation
{
    public class OrderInviteService:BaseService<OrderInvite, OrderInviteDTO, IRepository<OrderInvite>>, IOrderInviteService
    {
        public OrderInviteService(
            IUnitOfWork unitOfWork, 
            IRepository<OrderInvite> repository
            ) : base(unitOfWork, repository)
        {
        }

        public override void Create(OrderInviteDTO entity)
        {
            var dalEntity = MapToDalEntity(entity);
            Repository.Create(dalEntity);
            UnitOfWork.Commit();
            entity.Id = dalEntity.Id;
        }

        public override OrderInviteDTO GetById(int key)
        {
            var dalInvite = Repository.FindByIdWithTrack(key);
            var dtoInvite = MapToDtoEntity(dalInvite);
            return dtoInvite;
        }

        public IEnumerable<OrderInviteDTO> GetOrderInvitesById(int orderId)
        {
            var dalInvites = Repository.FindByPredicate(orderInv => orderInv.OrderId == orderId).ToList();
            var orderInvites = MapToDtoEntityList(dalInvites);
            return orderInvites;
        }   

        public IEnumerable<OrderInviteDTO> GetAccountInvitesByLogin(string login)
        {
            throw new System.NotImplementedException();
        }
    }
}