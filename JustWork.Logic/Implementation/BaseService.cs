﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using JustWork.Common;
using JustWork.Common.Exceptions;
using JustWork.DataProvider.Interface;
using JustWork.Logic.Interface;

namespace JustWork.Logic.Implementation
{
    public class BaseService<TDalEntity, TEntity, TRepository> : IService<TEntity>
        where TEntity : class, IEntity
        where TDalEntity : class, IEntity
        where TRepository : IRepository<TDalEntity>
    {
        protected readonly IUnitOfWork UnitOfWork;
        protected readonly TRepository Repository;

        public BaseService(
            IUnitOfWork unitOfWork,
            TRepository repository
            )
        {
            UnitOfWork = unitOfWork;
            Repository = repository;
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            var dalEntities = Repository.GetAll().ToList();
            return MapToDtoEntityList(dalEntities);
        }

        public virtual TEntity GetById(int key)
        {
            var dalUser = Repository.FindById(key);
            var entity = MapToDtoEntity(dalUser);
            return entity;
        }

        public virtual IEnumerable<TEntity> GetByPredicate(Expression<Func<TEntity, bool>> predicate)
        {
            if (predicate == null) throw new ItemNotFoundException(nameof(predicate));
            var predicateCompile = predicate.Compile();
            var entities = GetAll().Where(predicateCompile);
            return entities;
        }

        public virtual void Create(TEntity entity)
        {
            var dalEntity = MapToDalEntity(entity);
            Repository.Create(dalEntity);
            UnitOfWork.Commit();
        }

        public virtual void Delete(TEntity entity)
        {
            var dalEntity = Repository.FindByIdWithTrack(entity.Id);
            Repository.Remove(dalEntity);
            UnitOfWork.Commit();
        }

        public virtual void Update(TEntity entity)
        {
            var dalEntity = MapToDalEntity(entity);
            Repository.Update(dalEntity);
            UnitOfWork.Commit();
        }

        public int GetItemsCount()
        {
            return Repository.GetAll().Count();
        }

        protected virtual TDalEntity MapToDalEntity(TEntity dtoEntity)
        {
            return  Mapper.Map<TEntity, TDalEntity>(dtoEntity);
        }

        protected virtual TEntity MapToDtoEntity(TDalEntity dalEntity)
        {
            return Mapper.Map<TDalEntity, TEntity>(dalEntity);
        }

        protected virtual IEnumerable<TEntity> MapToDtoEntityList(IEnumerable<TDalEntity> dalList)
        {
            return Mapper.Map<IEnumerable<TDalEntity>, List<TEntity>>(dalList);
        }
    }
}