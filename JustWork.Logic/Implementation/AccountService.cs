﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using JustWork.Common;
using JustWork.DataProvider.DalModels;
using JustWork.DataProvider.Interface;
using JustWork.Logic.DTO;
using JustWork.Logic.Interface;

namespace JustWork.Logic.Implementation
{
    public class AccountService : BaseService<Account, AccountDTO, IRepository<Account>>, IAccountService
    {
        private readonly IUserService _userService;
        private readonly IService<RoleDTO> _roleService;

        public AccountService(
            IUnitOfWork unitOfWork,
            IRepository<Account> repository,
            IUserService userService, 
            IService<RoleDTO> roleService
            ) : base(unitOfWork, repository)
        {
            _userService = userService;
            _roleService = roleService;
        }

        public IEnumerable<AccountDTO> GetAccountByRole(string role)
        {
            var users = _userService.GetByRole(role);
            foreach (var user in users)
            {
                yield return GetByIdWithInclude(user.Id);
            }
        }

        public int GetCount(Expression<Func<Account, bool>> predicate)
        {
            return Repository.FindByPredicate(predicate).Count();
        }

        public Tuple<IEnumerable<AccountDTO>, int> GetByPredicate(Expression<Func<Account, bool>> predicate, int page)
        {
            var totalItemCount = Repository.FindByPredicate(predicate).Count();
            const int pageSize = ConfigurationsConstants.ItemCountOnPage;
            var skipCount = (page - 1) * pageSize;
            var dalList = Repository.FindByPredicate(predicate);

            var resultDalList = dalList.OrderByDescending(acc=> acc.Id).Skip(skipCount).Take(pageSize).ToList();
            var bllList = MapToDtoEntityList(resultDalList).ToList();
            var result = new Tuple<IEnumerable<AccountDTO>, int>(bllList, totalItemCount);

            return result;
        }

        public override AccountDTO GetById(int key)
        {
            var dalAcc = Repository.FindByIdWithTrack(key);
            return MapToDtoEntity(dalAcc);
        }

        public override void Create(AccountDTO entity)
        {
            var dalAcc = MapToDalEntity(entity);
            dalAcc.User = CreateUser(entity.UserDto);

            dalAcc.Name = entity.UserDto.Login;
            dalAcc.BirthDate = DateTime.Now;
            if (entity.UserDto.RoleDto.Id == (int)UserRoles.Employer)
            {
                dalAcc = InitializeEmployerAccount(dalAcc);
            }
            Repository.Create(dalAcc);
            UnitOfWork.Commit();
        }

        public override void Update(AccountDTO entity)
        {
            var newEntity = Repository.FindByIdWithTrack(entity.Id);
            Mapper.Map(entity, newEntity);
            Repository.Update(newEntity);
            UnitOfWork.Commit();
        }

        public override void Delete(AccountDTO entity)
        {
            var dalAccount = Repository.FindByIdWithTrack(entity.Id);

            DeleteUser(dalAccount?.User);
            base.Delete(entity);
        }

        public AccountDTO GetByUsernameWithInclude(string username)
        {
            var user = _userService.GetByLogin(username);

            if (user == null)
            {
                return  null;
            }

            var account = GetByIdWithInclude(user.Id);

            return account;
        }

        public AccountDTO GetByIdWithInclude(int id)
        {
            var dalAcc = Repository.FindByIdWithTrack(id);
            return MapToDtoEntity(dalAcc);
        }

        private static Account InitializeEmployerAccount(Account account)
        {
            account.Portfolio = new Portfolio();
            account.ServiceTypes = new HashSet<ServiceType>();
            account.Ratings = new HashSet<Rating>();
            account.UserPaids = new HashSet<UserPaid>();
            return account;
        }

        private static User CreateUser(UserDTO dtoUser)
        {
            return Mapper.Map<UserDTO, User>(dtoUser);
        }

        private void DeleteUser(User user)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<User, UserDTO>());
            var delUser = Mapper.Map<User, UserDTO>(user);
            _userService.Delete(delUser);
        }
    }
}