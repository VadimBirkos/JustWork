﻿using AutoMapper;
using JustWork.DataProvider.DalModels;
using JustWork.Logic.DTO;

namespace JustWork.Logic.AutoMapperProfiles
{
    public class UserProfile:Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>()
                .ForMember(dest => dest.RoleDto,
                    opts => opts.MapFrom(src => src.Role));

            CreateMap<UserDTO, User>()
                .ForMember(dest => dest.RoleId,
                    opts => opts.MapFrom(src => src.RoleDto.Id))
                .ForMember(dest => dest.Role,
                    opts => opts.Ignore());
        }
    }
}
