﻿using System;
using System.Collections.Generic;
using AutoMapper;

namespace JustWork.Logic.AutoMapperProfiles
{
    public class BllCommonCongif
    {
        public static List<Type> ConfigureBllMapper()
        {
            return new List<Type>()
            {
                typeof(AccountProfile),
                typeof(RatingProfile),
                typeof(OrderInviteProfile),
                typeof(UserProfile),
                typeof(OrderProfile),
                typeof(CommonTypesProfile)
            };
        }
    }
}