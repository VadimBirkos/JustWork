﻿using AutoMapper;
using JustWork.DataProvider.DalModels;
using JustWork.Logic.DTO;

namespace JustWork.Logic.AutoMapperProfiles
{
    public class OrderInviteProfile : Profile
    {
        public OrderInviteProfile()
        {
            CreateMap<OrderInviteDTO, OrderInvite>()
                .AfterMap((src, dest) =>
                dest.EmployerId = src.Employer.Id)
                .AfterMap((src, dest) =>
                dest.OrderId = src.Order.Id)
                .ForMember(dest => dest.Order,
                opts => opts.Ignore())
                .ForMember(dest => dest.Employer,
                opts => opts.Ignore());

            CreateMap<OrderInvite, OrderInviteDTO>()
                .ForMember(dest => dest.Employer,
                    opts => opts.MapFrom(src => src.Employer))
                .ForMember(dest => dest.Order,
                    opts => opts.MapFrom(
                            src => new OrderDTO
                            {
                                Id = src.OrderId,
                                OrderStateDto = Mapper.Map<OrderState, OrderStateDTO>(src.Order.OrderState),
                                Employer = Mapper.Map<Account, AccountDTO>(src.Order.Employer),
                                Customer = Mapper.Map<Account, AccountDTO>(src.Order.Customer)
                            }
                       ));
        }
    }
}