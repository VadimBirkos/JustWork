﻿using System.Collections.Generic;
using AutoMapper;
using JustWork.DataProvider.DalModels;
using JustWork.Logic.DTO;

namespace JustWork.Logic.AutoMapperProfiles
{
    public class CommonTypesProfile:Profile
    {
        public CommonTypesProfile()
        {
            CreateMap<Image, ImageDTO>();
            CreateMap<ImageDTO, Image>();

            CreateMap<OrderState, OrderStateDTO>();
            CreateMap<OrderStateDTO, OrderState>();

            CreateMap<Role, RoleDTO>();
            CreateMap<RoleDTO, Role>();

            CreateMap<PortfolioDTO, Portfolio>().ForAllMembers(opts => opts.Ignore());
            CreateMap<Portfolio, PortfolioDTO>().ForAllMembers(opts => opts.Ignore());

            CreateMap<ICollection<ServiceTypeDTO>, ICollection<ServiceType>>().ForAllMembers(opts => opts.Ignore()); ;
            CreateMap<ICollection<UserPaidDTO>, ICollection<UserPaid>>().ForAllMembers(opts => opts.Ignore());

            CreateMap<ICollection<ServiceType>, ICollection<ServiceTypeDTO>>();
            CreateMap<ICollection<UserPaid>, ICollection<UserPaidDTO>>();
        }
    }
}