﻿using AutoMapper;
using JustWork.DataProvider.DalModels;
using JustWork.Logic.DTO;

namespace JustWork.Logic.AutoMapperProfiles
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateMap<Order, OrderDTO>()
                .ForMember(dest => dest.OrderStateDto,
                    opts => opts.MapFrom(src => src.OrderState))
                .ForMember(dest => dest.OrderInvites,
                    opts => opts.MapFrom( src => src.OrderInvites))
                .ForMember(dest => dest.RatingsDto,
                    opts => opts.MapFrom(src => src.Ratings))
                .ForMember(dest => dest.ImageDto, 
                    opts => opts.MapFrom(src => src.Image))
                .AfterMap((src, dest) =>
                    dest.Customer = Mapper.Map<Account, AccountDTO>(src.Customer))
                .AfterMap((src, dest) =>
                {
                    if (dest.Employer == null) return;
                    dest.Employer = Mapper.Map<Account, AccountDTO>(src.Employer);
                });


            CreateMap<OrderDTO, Order>()
                .ForMember(dest => dest.Image,
                    opts => opts.Ignore())
                .ForMember(dest => dest.OrderStateId,
                    opts => opts.MapFrom(src => src.OrderStateDto.Id))
                .ForMember(dest => dest.CustomerId,
                    opts => opts.MapFrom(src => src.Customer.Id))
                .ForMember(dest => dest.OrderInvites,
                    opts => opts.Ignore())
                 .ForMember(dest => dest.Employer,
                 opts => opts.Ignore())
                .ForMember(dest => dest.Customer,
                    opts => opts.Ignore())
                .ForMember(dest => dest.OrderState,
                    opts => opts.Ignore())
                .AfterMap((src, dest) =>
                {
                    if (src.Employer != null)
                    {
                        dest.EmployerId = src.Employer.Id;
                    }
                })
                .AfterMap((src, dest) =>
                {
                    if (!string.IsNullOrEmpty(src.ImageDto?.ImagePath))
                    {
                        dest.Image = dest.Image ?? new Image();
                        dest.Image.ImagePath = src.ImageDto.ImagePath;
                    }
                });
        }
    }
}