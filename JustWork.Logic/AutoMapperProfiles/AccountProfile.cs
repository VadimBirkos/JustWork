﻿using AutoMapper;
using JustWork.DataProvider.DalModels;
using JustWork.Logic.DTO;

namespace JustWork.Logic.AutoMapperProfiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<Account, AccountDTO>()
                .ForMember(dest => dest.UserDto,
                    opts => opts.MapFrom(
                        src => src.User))
                .ForMember(dest => dest.Avatar,
                    opts => opts.MapFrom(src => src.Image))
                    .ForMember(dest => dest.RatingsDto,
                    opts => opts.MapFrom(src => src.Ratings))
                .ForMember(dest => dest.PortfolioDto,
                    opts => opts.Ignore())
                 .ForMember(dest => dest.OrderInvites,
                 opts => opts.Ignore());

            CreateMap<AccountDTO, Account>()
                .ForMember(dest => dest.Image,
                    opts => opts.MapFrom(src => src.Avatar))
                .ForMember(dest => dest.ImageId,
                    opts => opts.MapFrom(src => src.Avatar.Id))
                .ForMember(dest => dest.User,
                    opts => opts.MapFrom(src => src.UserDto))
                .ForMember(dest => dest.OrderInvites,
                    opts => opts.Ignore())
                .AfterMap((src, dest) =>
                    dest.User.RoleId = src.UserDto.RoleDto.Id);
        }
    }
}