﻿using AutoMapper;
using JustWork.DataProvider.DalModels;
using JustWork.Logic.DTO;

namespace JustWork.Logic.AutoMapperProfiles
{
    public class RatingProfile:Profile
    {
        public RatingProfile()
        {
            CreateMap<Rating, RatingDTO>()
                .ForMember(dest => dest.OrderDto,
                opts => opts.MapFrom(src => new OrderDTO()
                {
                    Id = src.OrderId,
                    Name = src.Order.Name
                }))
                .AfterMap((src, dest) => 
                dest.AccountDto = new AccountDTO
                {
                    Name = src.Account.Name,
                    UserDto = Mapper.Map<User, UserDTO>(src.Account.User),
                    Id = src.AccountId,
                    Avatar = new ImageDTO {
                        Id = src.Account.ImageId.Value,
                        ImagePath = src.Account.Image.ImagePath}
                });

            CreateMap<RatingDTO, Rating>()
                .ForMember(dest => dest.Order,
                opts => opts.Ignore())
                .ForMember(dest => dest.Account,
                opts => opts.Ignore())
                .AfterMap((src, dest) => dest.OrderId = src.OrderDto.Id)
                .AfterMap((src, dest) => dest.AccountId = src.AccountDto.Id);

        }
    }
}