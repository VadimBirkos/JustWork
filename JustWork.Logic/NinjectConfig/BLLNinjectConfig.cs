﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using JustWork.DataProvider.DalModels;
using JustWork.DataProvider.Interface;
using JustWork.Logic.DTO;
using JustWork.Logic.Implementation;
using JustWork.Logic.Interface;
using Ninject;

namespace JustWork.Logic.NinjectConfig
{
    public class BllNinjectConfig : IDependencyResolver
    {
        private readonly IKernel _kernel;

        public BllNinjectConfig(IKernel kernel)
        {
            _kernel = kernel;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return _kernel.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            _kernel.Bind<IService<RoleDTO>>().To<BaseService<Role, RoleDTO, IRepository<Role>>>();
            _kernel.Bind<IUserService>().To<UserService>();
            _kernel.Bind<IAccountService>().To<AccountService>();
            _kernel.Bind<IService<ImageDTO>>().To<BaseService<Image, ImageDTO, IRepository<Image>>>();
            _kernel.Bind<IOrderService>().To<OrderService>();
            _kernel.Bind<IService<OrderStateDTO>>()
                .To<BaseService<OrderState, OrderStateDTO, IRepository<OrderState>>>();
            _kernel.Bind<IOrderInviteService>().To<OrderInviteService>();
            _kernel.Bind<IRatingService>().To<RatingService>();
        }
    }
}
