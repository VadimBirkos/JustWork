﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using JustWork.DataProvider.DalModels;
using JustWork.Logic.DTO;

namespace JustWork.Logic.Interface
{
    public interface IRatingService:IService<RatingDTO>
    {
        IEnumerable<RatingDTO> GetRatingByOrder(int orderId);
        IEnumerable<RatingDTO> GetByPredicate(Expression<Func<Rating, bool>> predicate);
    }
}