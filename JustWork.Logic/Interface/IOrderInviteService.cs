﻿using System.Collections.Generic;
using JustWork.Logic.DTO;

namespace JustWork.Logic.Interface
{
    public interface IOrderInviteService:IService<OrderInviteDTO>
    {
        IEnumerable<OrderInviteDTO> GetOrderInvitesById(int orderId);
        IEnumerable<OrderInviteDTO> GetAccountInvitesByLogin(string login);
    }
}