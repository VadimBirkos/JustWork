﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using JustWork.DataProvider.DalModels;
using JustWork.Logic.DTO;

namespace JustWork.Logic.Interface
{
    public interface IUserService:IService<UserDTO>
    {
        UserDTO GetByLogin(string login);
        IEnumerable<UserDTO> GetByRole(string role);
        IEnumerable<UserDTO> GetByPredicate(Expression<Func<User, bool>> predicate);
    }
}