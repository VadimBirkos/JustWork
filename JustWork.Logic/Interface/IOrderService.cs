﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using JustWork.DataProvider.DalModels;
using JustWork.Logic.DTO;

namespace JustWork.Logic.Interface
{
    public interface IOrderService : IService<OrderDTO>
    {
        Tuple<IEnumerable<OrderDTO>, int> GetByPredicate(Expression<Func<Order, bool>> predicate, int page);
        Tuple<IEnumerable<OrderDTO>, int> SortByInvites(Expression<Func<Order, bool>> predicate, int? page);
        int GetItemsCount(Expression<Func<Order, bool>> predicate);
    }
}