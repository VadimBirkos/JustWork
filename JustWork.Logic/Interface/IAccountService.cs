﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using JustWork.DataProvider.DalModels;
using JustWork.Logic.DTO;

namespace JustWork.Logic.Interface
{
    public interface IAccountService:IService<AccountDTO>
    {
        AccountDTO GetByUsernameWithInclude(string username);
        AccountDTO GetByIdWithInclude(int id);
        IEnumerable<AccountDTO> GetAccountByRole(string role);
        int GetCount(Expression<Func<Account, bool>> predicate);
        Tuple<IEnumerable<AccountDTO>, int> GetByPredicate(Expression<Func<Account, bool>> predicate, int page);
    }
}