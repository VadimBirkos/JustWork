﻿namespace JustWork.Common
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}