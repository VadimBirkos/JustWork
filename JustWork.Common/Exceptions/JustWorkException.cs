﻿using System;

namespace JustWork.Common.Exceptions
{
    public class JustWorkException:ApplicationException
    {
        public JustWorkException()
        {
            
        }

        public JustWorkException(string message):base(message)
        {
            
        }
    }
}
