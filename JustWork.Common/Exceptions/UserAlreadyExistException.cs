﻿namespace JustWork.Common.Exceptions
{
    public class UserAlreadyExistException:JustWorkException
    {
        public UserAlreadyExistException()
        {
            
        }

        public UserAlreadyExistException(string message):base(message)
        {
            
        }
    }
}