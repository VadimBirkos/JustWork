﻿namespace JustWork.Common.Exceptions
{
    public class ItemNotFoundException:JustWorkException
    {
        public ItemNotFoundException()
        {
            
        }

        public ItemNotFoundException(string message):base(message)
        {
            
        }
    }
}