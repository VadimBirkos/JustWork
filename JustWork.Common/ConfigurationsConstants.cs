﻿namespace JustWork.Common
{
    public enum OrderStates
    {
        New = 1, 
        InProgress = 2, 
        Complete = 3, 
        Canceled = 4
    }

    public enum UserRoles
    {
        Admin = 1,
        Customer = 2,
        Employer = 3
    }

    public enum ByteFileSizes
    {
        Kilobyte = 1024,
        Megabyte = 1048576,
        Gigabyte = 1073741824
    }

    public class ConfigurationsConstants
    {
        public const int ItemCountOnPage = 5;
        public static  readonly  string[] ImageFormats = {".jpg", ".png", ".gif", ".bmp", ".jpeg"};

        public static string NewInviteMsg =
            "<h3>You get new invite in you order. <br> " +
            "Click on the below link to get more information: <br></h3>";

        public static string InviteChoiced =
            "<h3>Congratulations! Customer choiced your invite. <br>" +
            "Click on the below link to get more information: <br></h3>";

        public static string OrderStateChange =
            "<h3>Order, which you are performing, changed the state. <br> " +
            "Click on the below link to get more information: <br></h3>";
    }
}