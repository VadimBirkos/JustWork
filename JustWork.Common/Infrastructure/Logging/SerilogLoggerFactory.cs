﻿using System.Configuration;
using Serilog;
using SerilogWeb.Classic.Enrichers;

namespace JustWork.Common.Infrastructure.Logging
{
    public class SerilogLoggerFactory : ILoggerFactory
    {
        public ILogger CreateLogger()
        {
            // TODO: Refactor it if other solution to register Serilog and register it to Kernel found.
            var logsPath = ConfigurationManager.AppSettings["logsPath"];
            var logsSeqUrl = ConfigurationManager.AppSettings["logsSeqUrl"];

            var logger = new LoggerConfiguration()
                .Enrich.With<HttpRequestIdEnricher>()
                .WriteTo.RollingFile(logsPath)
                .WriteTo.Seq(logsSeqUrl)
                .CreateLogger();

            Log.Logger = logger;
            
            return new SerilogLogger(logger);
        }
    }
}
