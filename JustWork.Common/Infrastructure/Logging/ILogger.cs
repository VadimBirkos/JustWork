﻿using System;

namespace JustWork.Common.Infrastructure.Logging
{
    public interface ILogger
    {
        void Debug(string message);
        void Information(string message);
        void Error(string message);
        void Error(Exception exception, string template);
        void Fatal(string message);
    }
}
