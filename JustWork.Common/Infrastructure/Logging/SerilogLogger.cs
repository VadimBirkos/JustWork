﻿using System;

namespace JustWork.Common.Infrastructure.Logging
{
    public class SerilogLogger : ILogger
    {
        private readonly Serilog.ILogger _logger;

        public SerilogLogger(Serilog.ILogger serilogLogger)
        {
            _logger = serilogLogger;
        }
        
        public void Debug(string message)
        {
            _logger.Debug(message);
        }

        public void Information(string message)
        {
            _logger.Information(message);
        }

        public void Error(string message)
        {
            _logger.Error(message);
        }

        public void Error(Exception exception, string template)
        {
            _logger.Error(exception, template);
        }

        public void Fatal(string message)
        {
            _logger.Fatal(message);
        }
    }
}
    