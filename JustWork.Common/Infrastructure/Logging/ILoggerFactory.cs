﻿namespace JustWork.Common.Infrastructure.Logging
{
    public interface ILoggerFactory
    {
        ILogger CreateLogger();
    }
}
