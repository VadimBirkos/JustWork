﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using JustWork.DataProvider.DalModels;

namespace JustWork.DataProvider
{
    public class JustWorkContext : DbContext
    {
        public JustWorkContext() : base("JustWorkContext")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<UserPaid> UserPaids { get; set; }
        public DbSet<ServiceType> ServiceTypes { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<RaitingType> RaitingTypes { get; set; }
        public DbSet<PortfolioJob> PortfolioJobs { get; set; }
        public DbSet<Portfolio> Portfolios { get; set; }
        public DbSet<PaidType> PaidTypes { get; set; }
        public DbSet<OrderState> OrderStates { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<OrderInvite> OrderInvites { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PortfolioJob>()
                .HasMany(x => x.Images)
                .WithMany(x => x.PortfolioJobs)
                .Map(t =>
                {
                    t.MapLeftKey("PortfolioJobId");
                    t.MapRightKey("ImageId");
                    t.ToTable("PortfolioJobImage");
                });

            modelBuilder.Entity<Account>()
                .HasMany(x => x.ServiceTypes)
                .WithMany(x => x.Accounts)
                .Map(t =>
                    {
                        t.MapLeftKey("AccountId");
                        t.MapRightKey("ServiceTypeId");
                        t.ToTable("AccountServiceType");
                    });

            modelBuilder.Entity<Order>()
                .HasMany(x => x.ServiceTypes)
                .WithMany(x => x.Orders)
                .Map(t =>
                {
                    t.MapLeftKey("OrderId");
                    t.MapRightKey("ServiceTypeId");
                    t.ToTable("OrderServiceType");
                });
            modelBuilder.Entity<OrderState>()
                .Property(prop => prop.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            base.OnModelCreating(modelBuilder);
        }

        public void FixEfProviderServicesProblem()
        {
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}