﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using JustWork.Common;

namespace JustWork.DataProvider.Interface
{
    public interface IRepository<TEntity> where TEntity:class, IEntity
    {
        void Create(TEntity item);
        TEntity FindById(int id);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> FindByPredicate(Expression<Func<TEntity, bool>> predicate);
        void Remove(TEntity item);
        void Update(TEntity item);
        TEntity FindByIdWithTrack(int id);
    }
}