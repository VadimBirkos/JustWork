﻿using System;

namespace JustWork.DataProvider.Interface
{
    public interface IUnitOfWork:IDisposable
    {
        void Commit();
    }
}