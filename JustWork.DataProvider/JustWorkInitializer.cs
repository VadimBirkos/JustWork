﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Helpers;
using JustWork.Common;
using JustWork.DataProvider.DalModels;
using JustWork.DataProvider.Migrations;

namespace JustWork.DataProvider
{
    public class JustWorkInitializer: MigrateDatabaseToLatestVersion<JustWorkContext, Configuration>
    {
        #region initializatorVariables

        private Array enumValues = Enum.GetValues(typeof(OrderStates));

        private List<User> _customerList = new List<User>();
        private List<User> _employerList = new List<User>();

        private const string Lorem = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
Sed velit massa, pharetra quis aliquet vel, aliquam eleifend elit. Nullam lectus mauris, vestibulum non venenatis eu, 
eleifend ut neque. Nunc diam leo, aliquet eget imperdiet a, faucibus eu quam. Sed tortor elit, tempus nec fermentum malesuada, 
tristique in libero. Vestibulum accumsan velit ut nisl cursus consectetur eu quis mi. Fusce auctor, felis non tempor convallis, 
mi sem pulvinar leo, quis laoreet sapien quam at odio. In vestibulum, nisl vitae aliquam pellentesque, leo ex congue felis, sit 
amet gravida ligula ligula quis lectus. Donec quis suscipit nisl.

Sed ut bibendum felis. Aliquam erat volutpat. Aenean est nulla, malesuada vitae tincidunt sed, 
dignissim eu sapien. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
Mauris posuere, augue in facilisis auctor, eros est sollicitudin neque, ut sodales eros lectus eu tortor.
Nulla hendrerit ultricies massa. Mauris luctus ex metus, nec porttitor elit varius vel.
Nunc convallis finibus nisl, eu tempor dolor. Aenean vel orci aliquam.";

        private readonly List<string> _logins = new List<string>
        {
            "Bolendon", "Aholicleak", "Calamic", "Finaltypere",
            "Inesispe", "Maricapit", "Philleks", "Chetortq", "Gatechia",
            "Istagening", "Merciato", "Portruma", "Glorica", "Jewelfunker", "John", "Apolon",
            "Mercuri", "Reenbran", "Paroulli", "Giveallism", "Pectorce"
        };

        private const string DefaultPass = "123456";

        private readonly Random _random = new Random();

        private readonly List<string> _namesForOrder = new List<string>
        {
            "House cleaning",
            "Make website for autoshop",
            "Need a nanny for my childrens",
            "How to build a strong ",
            "make money with IoT",
            "The benefits and drawbacks",
            "Embassies and consulates in Moscow",
            "Need staff for my birthday",
            "Need cook in my restraunt",
            "Cleaner in hotel",
            "Clean the pipe",
            "Make pie for my brother",
            "Need organization company"
        };

        #endregion

        public override void InitializeDatabase(JustWorkContext context)
        {
            base.InitializeDatabase(context);
            context.Roles.AddOrUpdate(
                rl => rl.Title,
                new Role { Id = (int)UserRoles.Admin, Title = "admin" },
                new Role { Id = (int)UserRoles.Customer, Title = "customer" },
                new Role { Id = (int)UserRoles.Employer, Title = "employer" });

            context.OrderStates.AddOrUpdate(
                orSt => orSt.Id,
                new OrderState { Id = (int)OrderStates.New, Name = "New" },
                new OrderState { Id = (int)OrderStates.InProgress, Name = "In progress" },
                new OrderState { Id = (int)OrderStates.Complete, Name = "Complete" },
                new OrderState { Id = (int)OrderStates.Canceled, Name = "Canceled" }
            );
            context.SaveChanges();
            if(context.Users.Count() > 20) return;

            for (var i = 0; i < _logins.Count; i++)
            {
                var newUser = CreateNewUser(_logins[i], i);
                if (i % 2 == 0)
                {
                    newUser.RoleId = (int)UserRoles.Customer;
                    _customerList.Add(newUser);
                }
                else
                {
                    newUser.RoleId = (int)UserRoles.Employer;
                    _employerList.Add(newUser);
                }
            }

            var admin = CreateNewUser("Admin", 1);
            admin.RoleId = (int)UserRoles.Admin;

            foreach (var customer in _customerList)
            {
                context.Users.AddOrUpdate(prop => prop.Login,
                    customer);
            }
            foreach (var employ in _employerList)
            {
                context.Users.AddOrUpdate(prop => prop.Login,
                    employ);
            }
            context.Users.AddOrUpdate(prop => prop.Login,
                admin);

            context.SaveChanges();
            if (context.Orders.Count() <= 900)
            {
                GenerateOrders(context, 1000);
            }
        }

        private User CreateNewUser(string login, int index)
        {
            var newUser = new User
            {
                Account = new Account
                {
                    Name = login,
                    BirthDate = DateTime.Now.AddYears(_random.Next(-50, -15)),
                    AdditionalInfo = Lorem.Substring(_random.Next(10, 400)),
                    Image = new Image { ImagePath = "/Images/DefaultAvatars/avatar_"+index+".jpg" }
                },                
                CreationDate = DateTime.Now,
                Login = login.ToLower(),
                PasswordHash = Crypto.HashPassword(DefaultPass),
                Email = login+"@gmail.com"
            };
            return newUser;
        }

        private void GenerateOrders(JustWorkContext context, int count)
        {
            for (var i = 0; i < count; i++)
            {
                var order = new Order
                {
                    Description = Lorem.Substring(0, _random.Next(10, 400)),
                    StartDate = DateTime.Now,
                    CustomerId = _customerList[_random.Next(0, _customerList.Count)].Id,
                    Budget = _random.Next(1, 4000),
                    EndDate = DateTime.Now.AddDays(_random.Next(1, 800)),
                    Name = _namesForOrder[_random.Next(0, _namesForOrder.Count)],
                    Image = new Image()
                    
                };
                CompleteOrderCreate(order);
                context.Orders.Add(order);
            }

            context.SaveChanges();
        }

        private Order CompleteOrderCreate(Order order)
        {
            var choiceState = (int)enumValues.GetValue(_random.Next(0, enumValues.Length));
            order.OrderStateId = choiceState;
            order.OrderInvites = GenerateInvites();

            if (choiceState == (int)OrderStates.InProgress)
            {
                var employerInviteId = _random.Next(0, order.OrderInvites.Count);
                order.EmployerId = order.OrderInvites.ToList()[employerInviteId].EmployerId;
            }
            else if (choiceState == (int)OrderStates.Complete)
            {
                var employerInviteId = _random.Next(0, order.OrderInvites.Count);
                order.EmployerId = order.OrderInvites.ToList()[employerInviteId].EmployerId;
                order.Ratings = GenerateRating(order, 1, 5);
            }
            else if (choiceState == (int)OrderStates.Canceled)
            {
                var employerInviteId = _random.Next(0, order.OrderInvites.Count);
                order.EmployerId = order.OrderInvites.ToList()[employerInviteId].EmployerId;
                order.Ratings = GenerateRating(order, 1, 3);
            }
            return order;
        }

        private List<OrderInvite> GenerateInvites()
        {
            var count = _random.Next(1, _employerList.Count);
            var resultList = new List<OrderInvite>();
            for (var i = 0; i < count; i++)
            {
                resultList.Add(
                    new OrderInvite()
                    {
                        Cost = _random.Next(1, 200),
                        EmployerId = _employerList[i].Id,
                        Message = Lorem.Substring(0, _random.Next(10, 50)),
                    }
                );
            }
            return resultList;
        }

        private List<Rating> GenerateRating(Order order, int minMark, int maxMark)
        {
            var resultRatings = new List<Rating>
            {
                new Rating
                {
                    Message = Lorem.Substring(0, _random.Next(10, 60)),
                    AccountId = order.CustomerId,
                    OrderId = order.Id,
                    RatingValue = _random.Next(minMark, maxMark)
                },

                new Rating
                {
                    Message = Lorem.Substring(0, _random.Next(10, 60)),
                    AccountId = order.EmployerId.Value,
                    OrderId = order.Id,
                    RatingValue = _random.Next(minMark, maxMark)
                }
            };
            return resultRatings;
        }
    }
}