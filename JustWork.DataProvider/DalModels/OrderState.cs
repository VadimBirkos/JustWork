﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using JustWork.Common;

namespace JustWork.DataProvider.DalModels
{
    [Table("OrderState")]
    public class OrderState:IEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Order> Orders { get; set; }

        public OrderState()
        {
            Orders = new HashSet<Order>();
        }
    }
}