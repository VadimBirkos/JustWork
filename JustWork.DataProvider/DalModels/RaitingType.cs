﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using JustWork.Common;

namespace JustWork.DataProvider.DalModels
{
    [Table("RaitingType")]
    public class RaitingType:IEntity
    {
        public int Id { get; set; }
        public string Type { get; set; }

       // public virtual ICollection<Rating> Ratings { get; set; }
    }
}