﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using JustWork.Common;

namespace JustWork.DataProvider.DalModels
{
    [Table("Order")]
    public class Order:IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public double Budget { get; set; }

        public int OrderStateId { get; set; }
        public virtual OrderState OrderState { get; set; }

        public virtual Account Employer { get; set; }
        [Column("EmployerId")]
        public int? EmployerId { get; set; }

        public virtual Image Image { get; set; }
        [Column("ImageId")]
        public int? ImageId { get; set; }

        public virtual Account Customer { get; set; }
        [Required]
        [Column("CustomerId")]
        public int  CustomerId { get; set; }

        public virtual ICollection<OrderInvite> OrderInvites{ get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
        public virtual ICollection<ServiceType> ServiceTypes { get; set; }

        public Order()
        {
            Ratings = new HashSet<Rating>();
            ServiceTypes = new HashSet<ServiceType>();
            OrderInvites = new HashSet<OrderInvite>();
        }
    }
}