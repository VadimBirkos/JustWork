﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using JustWork.Common;

namespace JustWork.DataProvider.DalModels
{
    [Table("ServiceType")]
    public class ServiceType:IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Account> Accounts { get; set; }
        public virtual ICollection<Order> Orders { get; set; }

        public ServiceType()
        {
            Accounts = new HashSet<Account>();
            Orders = new HashSet<Order>();
        }
    }
}