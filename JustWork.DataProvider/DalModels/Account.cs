﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using JustWork.Common;

namespace JustWork.DataProvider.DalModels
{
    [Table("Account")]
    public class Account:IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AdditionalInfo { get; set; }
        
        public DateTime? BirthDate { get; set; }

        public virtual User User { get; set; }

        public int? ImageId { get; set; }
        public virtual Image Image { get; set; }

        public virtual Portfolio Portfolio { get; set; }

        public virtual ICollection<OrderInvite> OrderInvites { get; set; }
        public virtual ICollection<ServiceType> ServiceTypes { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
        public virtual ICollection<UserPaid> UserPaids { get; set; }
    }
}