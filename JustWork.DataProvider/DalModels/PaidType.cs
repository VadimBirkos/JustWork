﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using JustWork.Common;

namespace JustWork.DataProvider.DalModels
{
    [Table("PaidType")]
    public class PaidType:IEntity
    {
        public int Id { get; set; }
        public string Type { get; set; }

        public virtual ICollection<UserPaid> UserPaids { get; set; }

        public PaidType()
        {
            UserPaids = new HashSet<UserPaid>();
        }
    }
}