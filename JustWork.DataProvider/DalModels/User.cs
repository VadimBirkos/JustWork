﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using JustWork.Common;

namespace JustWork.DataProvider.DalModels
{
    [Table("User")]
    public class User:IEntity
    {
        [Key, ForeignKey("Account")]
        public int Id { get; set; }
        public string Login { get; set; }
        public string PasswordHash { get; set; }
        public DateTime CreationDate { get; set; }
        public string Email { get; set; }
        public bool ConfirmEmail { get; set; }

        public int? RoleId { get; set; }
        public virtual Role Role { get; set; }

        public virtual Account Account { get; set; }
    }
}
