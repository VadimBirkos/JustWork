﻿using System.ComponentModel.DataAnnotations.Schema;
using JustWork.Common;

namespace JustWork.DataProvider.DalModels
{
    [Table("Rating")]
    public class Rating:IEntity
    {
        public int Id { get; set; }
        public double RatingValue { get; set; }
        public string Message { get; set; }

        public int OrderId { get; set; }
        public virtual Order Order { get; set; }

        public int AccountId { get; set; }
        public virtual Account Account { get; set; }
    }
}