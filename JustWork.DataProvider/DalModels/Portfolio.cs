﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using JustWork.Common;

namespace JustWork.DataProvider.DalModels
{
    [Table("Portfolio")]
    public class Portfolio:IEntity
    {
        [Key]
        [ForeignKey("Account")]
        public int Id { get; set; }
        public string Description { get; set; }

        public Account Account { get; set; }

        public virtual ICollection<PortfolioJob> Jobs { get; set; }

        public Portfolio()
        {
            Jobs = new HashSet<PortfolioJob>();
        }
    }
}