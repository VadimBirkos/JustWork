﻿using System.ComponentModel.DataAnnotations.Schema;
using JustWork.Common;

namespace JustWork.DataProvider.DalModels
{
    [Table("UserPaid")]
    public class UserPaid:IEntity
    {
        public int Id { get; set; }
        public double CostForType { get; set; }

        public int? PaidTypeId { get; set; }
        public virtual PaidType PaidType { get; set; }

        public int? AccountId { get; set; }
        public virtual Account Account { get; set; }
    }
}