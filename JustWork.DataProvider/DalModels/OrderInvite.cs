﻿using System.ComponentModel.DataAnnotations.Schema;
using JustWork.Common;

namespace JustWork.DataProvider.DalModels
{
    public class OrderInvite:IEntity
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public double Cost { get; set; }

        public virtual Account Employer{ get; set; }
        [Column("EmployerId")]
        public int EmployerId{ get; set; }

        public virtual Order Order { get; set; }
        [Column("OrderId")]
        public int OrderId { get; set; }
    }
}
