﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using JustWork.Common;

namespace JustWork.DataProvider.DalModels
{
    [Table("PortfolioJob")]
    public class PortfolioJob:IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int TeamSize { get; set; }

        public int? PortfolioId { get; set; }
        public virtual Portfolio Portfolio { get; set; }

        public virtual ICollection<Image> Images { get; set; }

        public PortfolioJob()
        {
            Images = new HashSet<Image>();
        }
    }
}