﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using JustWork.Common;

namespace JustWork.DataProvider.DalModels
{
    [Table("Image")]
    public class Image:IEntity  
    {
        public int Id { get; set; }
        public string ImagePath { get; set; }

        public virtual ICollection<PortfolioJob> PortfolioJobs { get; set; }
        public virtual ICollection<Account> Accounts { get; set; }
        public virtual ICollection<Order> Orders { get; set; }

        public Image()
        {
            Orders = new HashSet<Order>();
            PortfolioJobs = new HashSet<PortfolioJob>();
            Accounts = new HashSet<Account>();
        }
    }
}