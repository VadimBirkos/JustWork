﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Mvc;
using JustWork.DataProvider.DalModels;
using JustWork.DataProvider.Implementation;
using JustWork.DataProvider.Interface;
using Ninject;
using Ninject.Web.Common;


namespace JustWork.DataProvider.NinjectConfig
{
    public class DalNinjectConfig:IDependencyResolver
    {
        private readonly IKernel _kernel;

        public DalNinjectConfig(IKernel kernel)
        {
            _kernel = kernel;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return _kernel.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>().InRequestScope();
            _kernel.Bind<DbContext>().To<JustWorkContext>().InRequestScope();

            _kernel.Bind<IRepository<Account>>().To<BaseRepository<Account>>();
            _kernel.Bind<IRepository<Image>>().To<BaseRepository<Image>>();
            _kernel.Bind<IRepository<Order>>().To<BaseRepository<Order>>();
            _kernel.Bind<IRepository<OrderState>>().To<BaseRepository<OrderState>>();
            _kernel.Bind<IRepository<Rating>>().To<BaseRepository<Rating>>();
            _kernel.Bind<IRepository<Role>>().To<BaseRepository<Role>>();
            _kernel.Bind<IRepository<User>>().To<BaseRepository<User>>();
            _kernel.Bind<IRepository<OrderInvite>>().To<BaseRepository<OrderInvite>>();
        }
    }
}