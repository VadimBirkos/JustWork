namespace JustWork.DataProvider.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixRatingFieldName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Rating", "Message", c => c.String());
            DropColumn("dbo.Rating", "Meaasage");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Rating", "Meaasage", c => c.String());
            DropColumn("dbo.Rating", "Message");
        }
    }
}
