using System.Data.Entity.Migrations;

namespace JustWork.DataProvider.Migrations
{
    public partial class fixacc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Account", "Name");
        }
    }
}
