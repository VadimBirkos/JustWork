using System.Data.Entity.Migrations;
using System.Linq;
using JustWork.DataProvider.Interface;
using Ninject;

namespace JustWork.DataProvider.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<JustWorkContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }
    }
}
