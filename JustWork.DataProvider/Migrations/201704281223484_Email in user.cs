namespace JustWork.DataProvider.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Emailinuser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "Email", c => c.String());
            AddColumn("dbo.User", "ConfirmEmail", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.User", "ConfirmEmail");
            DropColumn("dbo.User", "Email");
        }
    }
}
