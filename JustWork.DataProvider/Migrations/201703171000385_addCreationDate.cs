namespace JustWork.DataProvider.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCreationDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "CreationDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.User", "CreationDate");
        }
    }
}
