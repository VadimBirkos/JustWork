namespace JustWork.DataProvider.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dateFix : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Account", "BirthDate", c => c.DateTime());
            AlterColumn("dbo.Order", "EndDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Order", "EndDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Account", "BirthDate", c => c.DateTime(nullable: false));
        }
    }
}
