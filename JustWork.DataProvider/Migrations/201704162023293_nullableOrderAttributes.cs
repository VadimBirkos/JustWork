namespace JustWork.DataProvider.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nullableOrderAttributes : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Order", "OrderStateId", "dbo.OrderState");
            DropIndex("dbo.Order", new[] { "OrderStateId" });
            AlterColumn("dbo.Order", "EndDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Order", "Budget", c => c.Double(nullable: false));
            AlterColumn("dbo.Order", "OrderStateId", c => c.Int(nullable: false));
            CreateIndex("dbo.Order", "OrderStateId");
            AddForeignKey("dbo.Order", "OrderStateId", "dbo.OrderState", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Order", "OrderStateId", "dbo.OrderState");
            DropIndex("dbo.Order", new[] { "OrderStateId" });
            AlterColumn("dbo.Order", "OrderStateId", c => c.Int());
            AlterColumn("dbo.Order", "Budget", c => c.Double());
            AlterColumn("dbo.Order", "EndDate", c => c.DateTime());
            CreateIndex("dbo.Order", "OrderStateId");
            AddForeignKey("dbo.Order", "OrderStateId", "dbo.OrderState", "Id");
        }
    }
}
