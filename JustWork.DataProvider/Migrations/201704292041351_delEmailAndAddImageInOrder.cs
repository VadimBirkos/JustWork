namespace JustWork.DataProvider.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class delEmailAndAddImageInOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Order", "ImageId", c => c.Int());
            CreateIndex("dbo.Order", "ImageId");
            AddForeignKey("dbo.Order", "ImageId", "dbo.Image", "Id");
            DropColumn("dbo.Account", "Email");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Account", "Email", c => c.String());
            DropForeignKey("dbo.Order", "ImageId", "dbo.Image");
            DropIndex("dbo.Order", new[] { "ImageId" });
            DropColumn("dbo.Order", "ImageId");
        }
    }
}
