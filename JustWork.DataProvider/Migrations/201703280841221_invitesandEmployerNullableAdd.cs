namespace JustWork.DataProvider.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class invitesandEmployerNullableAdd : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Order", "EmployerId", "dbo.Account");
            DropIndex("dbo.Order", new[] { "EmployerId" });
            CreateTable(
                "dbo.OrderInvites",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        Employer_Id = c.Int(),
                        Order_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Account", t => t.Employer_Id)
                .ForeignKey("dbo.Order", t => t.Order_Id)
                .Index(t => t.Employer_Id)
                .Index(t => t.Order_Id);
            
            AddColumn("dbo.Order", "Budget", c => c.Double());
            AlterColumn("dbo.Order", "EmployerId", c => c.Int());
            CreateIndex("dbo.Order", "EmployerId");
            AddForeignKey("dbo.Order", "EmployerId", "dbo.Account", "Id");
            DropColumn("dbo.Order", "Cost");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Order", "Cost", c => c.Double());
            DropForeignKey("dbo.Order", "EmployerId", "dbo.Account");
            DropForeignKey("dbo.OrderInvites", "Order_Id", "dbo.Order");
            DropForeignKey("dbo.OrderInvites", "Employer_Id", "dbo.Account");
            DropIndex("dbo.Order", new[] { "EmployerId" });
            DropIndex("dbo.OrderInvites", new[] { "Order_Id" });
            DropIndex("dbo.OrderInvites", new[] { "Employer_Id" });
            AlterColumn("dbo.Order", "EmployerId", c => c.Int(nullable: false));
            DropColumn("dbo.Order", "Budget");
            DropTable("dbo.OrderInvites");
            CreateIndex("dbo.Order", "EmployerId");
            AddForeignKey("dbo.Order", "EmployerId", "dbo.Account", "Id", cascadeDelete: true);
        }
    }
}
