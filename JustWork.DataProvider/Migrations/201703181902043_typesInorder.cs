namespace JustWork.DataProvider.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class typesInorder : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderServiceType",
                c => new
                    {
                        OrderId = c.Int(nullable: false),
                        ServiceTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.OrderId, t.ServiceTypeId })
                .ForeignKey("dbo.Order", t => t.OrderId)
                .ForeignKey("dbo.ServiceType", t => t.ServiceTypeId)
                .Index(t => t.OrderId)
                .Index(t => t.ServiceTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderServiceType", "ServiceTypeId", "dbo.ServiceType");
            DropForeignKey("dbo.OrderServiceType", "OrderId", "dbo.Order");
            DropIndex("dbo.OrderServiceType", new[] { "ServiceTypeId" });
            DropIndex("dbo.OrderServiceType", new[] { "OrderId" });
            DropTable("dbo.OrderServiceType");
        }
    }
}
