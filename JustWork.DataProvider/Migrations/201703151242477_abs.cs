using System.Data.Entity.Migrations;

namespace JustWork.DataProvider.Migrations
{
    public partial class abs : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AccountServiceType", "AccountId", "dbo.Account");
            DropForeignKey("dbo.AccountServiceType", "ServiceTypeId", "dbo.ServiceType");
            DropForeignKey("dbo.PortfolioJobImage", "PortfolioJobId", "dbo.PortfolioJob");
            DropForeignKey("dbo.PortfolioJobImage", "ImageId", "dbo.Image");
            AddForeignKey("dbo.AccountServiceType", "AccountId", "dbo.Account", "Id");
            AddForeignKey("dbo.AccountServiceType", "ServiceTypeId", "dbo.ServiceType", "Id");
            AddForeignKey("dbo.PortfolioJobImage", "PortfolioJobId", "dbo.PortfolioJob", "Id");
            AddForeignKey("dbo.PortfolioJobImage", "ImageId", "dbo.Image", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PortfolioJobImage", "ImageId", "dbo.Image");
            DropForeignKey("dbo.PortfolioJobImage", "PortfolioJobId", "dbo.PortfolioJob");
            DropForeignKey("dbo.AccountServiceType", "ServiceTypeId", "dbo.ServiceType");
            DropForeignKey("dbo.AccountServiceType", "AccountId", "dbo.Account");
            AddForeignKey("dbo.PortfolioJobImage", "ImageId", "dbo.Image", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PortfolioJobImage", "PortfolioJobId", "dbo.PortfolioJob", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AccountServiceType", "ServiceTypeId", "dbo.ServiceType", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AccountServiceType", "AccountId", "dbo.Account", "Id", cascadeDelete: true);
        }
    }
}
