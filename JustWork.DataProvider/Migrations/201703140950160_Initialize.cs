using System.Data.Entity.Migrations;

namespace JustWork.DataProvider.Migrations
{
    public partial class Initialize : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Account",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        AdditionalInfo = c.String(),
                        BirthDate = c.DateTime(nullable: false),
                        ImageId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Image", t => t.ImageId)
                .Index(t => t.ImageId);
            
            CreateTable(
                "dbo.Image",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ImagePath = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PortfolioJob",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        TeamSize = c.Int(nullable: false),
                        PortfolioId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Portfolio", t => t.PortfolioId)
                .Index(t => t.PortfolioId);
            
            CreateTable(
                "dbo.Portfolio",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Account", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Rating",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Raiting = c.Int(nullable: false),
                        Meaasage = c.String(),
                        RaitingTypeId = c.Int(),
                        OrderId = c.Int(),
                        AccountId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Account", t => t.AccountId)
                .ForeignKey("dbo.Order", t => t.OrderId)
                .ForeignKey("dbo.RaitingType", t => t.RaitingTypeId)
                .Index(t => t.RaitingTypeId)
                .Index(t => t.OrderId)
                .Index(t => t.AccountId);
            
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        OrderStateId = c.Int(),
                        Customer_Id = c.Int(),
                        Employer_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Account", t => t.Customer_Id)
                .ForeignKey("dbo.Account", t => t.Employer_Id)
                .ForeignKey("dbo.OrderState", t => t.OrderStateId)
                .Index(t => t.OrderStateId)
                .Index(t => t.Customer_Id)
                .Index(t => t.Employer_Id);
            
            CreateTable(
                "dbo.OrderState",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RaitingType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ServiceType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Login = c.String(),
                        PasswordHash = c.String(),
                        RoleId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Account", t => t.Id)
                .ForeignKey("dbo.Role", t => t.RoleId)
                .Index(t => t.Id)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: false),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserPaid",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CostForType = c.Double(nullable: false),
                        PaidTypeId = c.Int(),
                        AccountId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Account", t => t.AccountId)
                .ForeignKey("dbo.PaidType", t => t.PaidTypeId)
                .Index(t => t.PaidTypeId)
                .Index(t => t.AccountId);
            
            CreateTable(
                "dbo.PaidType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PortfolioJobImage",
                c => new
                    {
                        PortfolioJobId = c.Int(nullable: false),
                        ImageId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PortfolioJobId, t.ImageId })
                .ForeignKey("dbo.PortfolioJob", t => t.PortfolioJobId, cascadeDelete: true)
                .ForeignKey("dbo.Image", t => t.ImageId, cascadeDelete: true)
                .Index(t => t.PortfolioJobId)
                .Index(t => t.ImageId);
            
            CreateTable(
                "dbo.AccountServiceType",
                c => new
                    {
                        AccountId = c.Int(nullable: false),
                        ServiceTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AccountId, t.ServiceTypeId })
                .ForeignKey("dbo.Account", t => t.AccountId, cascadeDelete: true)
                .ForeignKey("dbo.ServiceType", t => t.ServiceTypeId, cascadeDelete: true)
                .Index(t => t.AccountId)
                .Index(t => t.ServiceTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserPaid", "PaidTypeId", "dbo.PaidType");
            DropForeignKey("dbo.UserPaid", "AccountId", "dbo.Account");
            DropForeignKey("dbo.User", "RoleId", "dbo.Role");
            DropForeignKey("dbo.User", "Id", "dbo.Account");
            DropForeignKey("dbo.AccountServiceType", "ServiceTypeId", "dbo.ServiceType");
            DropForeignKey("dbo.AccountServiceType", "AccountId", "dbo.Account");
            DropForeignKey("dbo.Rating", "RaitingTypeId", "dbo.RaitingType");
            DropForeignKey("dbo.Rating", "OrderId", "dbo.Order");
            DropForeignKey("dbo.Order", "OrderStateId", "dbo.OrderState");
            DropForeignKey("dbo.Order", "Employer_Id", "dbo.Account");
            DropForeignKey("dbo.Order", "Customer_Id", "dbo.Account");
            DropForeignKey("dbo.Rating", "AccountId", "dbo.Account");
            DropForeignKey("dbo.PortfolioJob", "PortfolioId", "dbo.Portfolio");
            DropForeignKey("dbo.Portfolio", "Id", "dbo.Account");
            DropForeignKey("dbo.PortfolioJobImage", "ImageId", "dbo.Image");
            DropForeignKey("dbo.PortfolioJobImage", "PortfolioJobId", "dbo.PortfolioJob");
            DropForeignKey("dbo.Account", "ImageId", "dbo.Image");
            DropIndex("dbo.AccountServiceType", new[] { "ServiceTypeId" });
            DropIndex("dbo.AccountServiceType", new[] { "AccountId" });
            DropIndex("dbo.PortfolioJobImage", new[] { "ImageId" });
            DropIndex("dbo.PortfolioJobImage", new[] { "PortfolioJobId" });
            DropIndex("dbo.UserPaid", new[] { "AccountId" });
            DropIndex("dbo.UserPaid", new[] { "PaidTypeId" });
            DropIndex("dbo.User", new[] { "RoleId" });
            DropIndex("dbo.User", new[] { "Id" });
            DropIndex("dbo.Order", new[] { "Employer_Id" });
            DropIndex("dbo.Order", new[] { "Customer_Id" });
            DropIndex("dbo.Order", new[] { "OrderStateId" });
            DropIndex("dbo.Rating", new[] { "AccountId" });
            DropIndex("dbo.Rating", new[] { "OrderId" });
            DropIndex("dbo.Rating", new[] { "RaitingTypeId" });
            DropIndex("dbo.Portfolio", new[] { "Id" });
            DropIndex("dbo.PortfolioJob", new[] { "PortfolioId" });
            DropIndex("dbo.Account", new[] { "ImageId" });
            DropTable("dbo.AccountServiceType");
            DropTable("dbo.PortfolioJobImage");
            DropTable("dbo.PaidType");
            DropTable("dbo.UserPaid");
            DropTable("dbo.Role");
            DropTable("dbo.User");
            DropTable("dbo.ServiceType");
            DropTable("dbo.RaitingType");
            DropTable("dbo.OrderState");
            DropTable("dbo.Order");
            DropTable("dbo.Rating");
            DropTable("dbo.Portfolio");
            DropTable("dbo.PortfolioJob");
            DropTable("dbo.Image");
            DropTable("dbo.Account");
        }
    }
}
