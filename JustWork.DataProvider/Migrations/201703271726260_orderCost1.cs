namespace JustWork.DataProvider.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderCost1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Order", "Cost", c => c.Double());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Order", "Cost", c => c.Int());
        }
    }
}
