namespace JustWork.DataProvider.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ratingAccountNavProp : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Rating", "AccountId", "dbo.Account");
            DropForeignKey("dbo.Rating", "OrderId", "dbo.Order");
            DropIndex("dbo.Rating", new[] { "OrderId" });
            DropIndex("dbo.Rating", new[] { "AccountId" });
            AlterColumn("dbo.Rating", "OrderId", c => c.Int(nullable: false));
            AlterColumn("dbo.Rating", "AccountId", c => c.Int(nullable: false));
            CreateIndex("dbo.Rating", "OrderId");
            CreateIndex("dbo.Rating", "AccountId");
            AddForeignKey("dbo.Rating", "AccountId", "dbo.Account", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Rating", "OrderId", "dbo.Order", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rating", "OrderId", "dbo.Order");
            DropForeignKey("dbo.Rating", "AccountId", "dbo.Account");
            DropIndex("dbo.Rating", new[] { "AccountId" });
            DropIndex("dbo.Rating", new[] { "OrderId" });
            AlterColumn("dbo.Rating", "AccountId", c => c.Int());
            AlterColumn("dbo.Rating", "OrderId", c => c.Int());
            CreateIndex("dbo.Rating", "AccountId");
            CreateIndex("dbo.Rating", "OrderId");
            AddForeignKey("dbo.Rating", "OrderId", "dbo.Order", "Id");
            AddForeignKey("dbo.Rating", "AccountId", "dbo.Account", "Id");
        }
    }
}
