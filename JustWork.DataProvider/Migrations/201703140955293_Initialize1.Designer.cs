// <auto-generated />

using System.CodeDom.Compiler;
using System.Data.Entity.Migrations.Infrastructure;
using System.Resources;

namespace JustWork.DataProvider.Migrations
{
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Initialize1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Initialize1));
        
        string IMigrationMetadata.Id
        {
            get { return "201703140955293_Initialize1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
