using System.Data.Entity.Migrations;

namespace JustWork.DataProvider.Migrations
{
    public partial class Initialize1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Order", "Customer_Id", "dbo.Account");
            DropForeignKey("dbo.Order", "Employer_Id", "dbo.Account");
            DropIndex("dbo.Order", new[] { "Customer_Id" });
            DropIndex("dbo.Order", new[] { "Employer_Id" });
            RenameColumn(table: "dbo.Order", name: "Customer_Id", newName: "CustomerId");
            RenameColumn(table: "dbo.Order", name: "Employer_Id", newName: "EmployerId");
            AlterColumn("dbo.Order", "CustomerId", c => c.Int(nullable: false));
            AlterColumn("dbo.Order", "EmployerId", c => c.Int(nullable: false));
            CreateIndex("dbo.Order", "EmployerId");
            CreateIndex("dbo.Order", "CustomerId");
            AddForeignKey("dbo.Order", "CustomerId", "dbo.Account", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Order", "EmployerId", "dbo.Account", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Order", "EmployerId", "dbo.Account");
            DropForeignKey("dbo.Order", "CustomerId", "dbo.Account");
            DropIndex("dbo.Order", new[] { "CustomerId" });
            DropIndex("dbo.Order", new[] { "EmployerId" });
            AlterColumn("dbo.Order", "EmployerId", c => c.Int());
            AlterColumn("dbo.Order", "CustomerId", c => c.Int());
            RenameColumn(table: "dbo.Order", name: "EmployerId", newName: "Employer_Id");
            RenameColumn(table: "dbo.Order", name: "CustomerId", newName: "Customer_Id");
            CreateIndex("dbo.Order", "Employer_Id");
            CreateIndex("dbo.Order", "Customer_Id");
            AddForeignKey("dbo.Order", "Employer_Id", "dbo.Account", "Id");
            AddForeignKey("dbo.Order", "Customer_Id", "dbo.Account", "Id");
        }
    }
}
