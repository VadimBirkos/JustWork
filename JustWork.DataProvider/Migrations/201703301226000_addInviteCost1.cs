namespace JustWork.DataProvider.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addInviteCost1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderInvites", "Employer_Id", "dbo.Account");
            DropForeignKey("dbo.OrderInvites", "Order_Id", "dbo.Order");
            DropIndex("dbo.OrderInvites", new[] { "Employer_Id" });
            DropIndex("dbo.OrderInvites", new[] { "Order_Id" });
            RenameColumn(table: "dbo.OrderInvites", name: "Employer_Id", newName: "EmployerId");
            RenameColumn(table: "dbo.OrderInvites", name: "Order_Id", newName: "OrderId");
            AlterColumn("dbo.OrderInvites", "EmployerId", c => c.Int(nullable: false));
            AlterColumn("dbo.OrderInvites", "OrderId", c => c.Int(nullable: false));
            CreateIndex("dbo.OrderInvites", "EmployerId");
            CreateIndex("dbo.OrderInvites", "OrderId");
            AddForeignKey("dbo.OrderInvites", "EmployerId", "dbo.Account", "Id", cascadeDelete: true);
            AddForeignKey("dbo.OrderInvites", "OrderId", "dbo.Order", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderInvites", "OrderId", "dbo.Order");
            DropForeignKey("dbo.OrderInvites", "EmployerId", "dbo.Account");
            DropIndex("dbo.OrderInvites", new[] { "OrderId" });
            DropIndex("dbo.OrderInvites", new[] { "EmployerId" });
            AlterColumn("dbo.OrderInvites", "OrderId", c => c.Int());
            AlterColumn("dbo.OrderInvites", "EmployerId", c => c.Int());
            RenameColumn(table: "dbo.OrderInvites", name: "OrderId", newName: "Order_Id");
            RenameColumn(table: "dbo.OrderInvites", name: "EmployerId", newName: "Employer_Id");
            CreateIndex("dbo.OrderInvites", "Order_Id");
            CreateIndex("dbo.OrderInvites", "Employer_Id");
            AddForeignKey("dbo.OrderInvites", "Order_Id", "dbo.Order", "Id");
            AddForeignKey("dbo.OrderInvites", "Employer_Id", "dbo.Account", "Id");
        }
    }
}
