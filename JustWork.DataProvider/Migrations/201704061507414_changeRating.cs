namespace JustWork.DataProvider.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeRating : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Rating", "RaitingTypeId", "dbo.RaitingType");
            DropIndex("dbo.Rating", new[] { "RaitingTypeId" });
            DropColumn("dbo.Rating", "RaitingTypeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Rating", "RaitingTypeId", c => c.Int());
            CreateIndex("dbo.Rating", "RaitingTypeId");
            AddForeignKey("dbo.Rating", "RaitingTypeId", "dbo.RaitingType", "Id");
        }
    }
}
