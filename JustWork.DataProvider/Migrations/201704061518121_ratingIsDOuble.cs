namespace JustWork.DataProvider.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ratingIsDOuble : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Rating", "Raiting", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Rating", "Raiting", c => c.Int(nullable: false));
        }
    }
}
