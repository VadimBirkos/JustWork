namespace JustWork.DataProvider.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addInviteCost : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderInvites", "Cost", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderInvites", "Cost");
        }
    }
}
