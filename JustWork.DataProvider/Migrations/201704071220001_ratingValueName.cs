namespace JustWork.DataProvider.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ratingValueName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Rating", "RatingValue", c => c.Double(nullable: false));
            DropColumn("dbo.Rating", "Raiting");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Rating", "Raiting", c => c.Double(nullable: false));
            DropColumn("dbo.Rating", "RatingValue");
        }
    }
}
