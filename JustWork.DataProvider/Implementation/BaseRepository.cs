﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using JustWork.Common;
using JustWork.DataProvider.Interface;

namespace JustWork.DataProvider.Implementation
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
    {
        private readonly DbContext _dbContext;
        private readonly DbSet<TEntity> _dbSet;

        public BaseRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<TEntity>();
        }

        public void Create(TEntity item)
        {
            _dbSet.Add(item);
        }

        public TEntity FindById(int id)
        {
            return _dbSet.AsNoTracking().FirstOrDefault(entity => entity.Id == id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.AsNoTracking();
        }

        public IEnumerable<TEntity> FindByPredicate(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Where(predicate).Select(i => i);
        }

        public void Remove(TEntity item)
        {
            _dbSet.Remove(item);
        }

        public virtual void Update(TEntity item)
        {
            if (_dbContext.Entry(item).State == EntityState.Detached)
            {
                var properties = typeof(TEntity).GetProperties();
                foreach (var property in properties)
                {
                    var containInterface = property.PropertyType.GetInterfaces().Contains(typeof(IEntity));
                    if (!containInterface) continue;

                    var entity = (IEntity)property.GetValue(item);
                    if (entity == null || _dbContext.Entry(entity).State != EntityState.Detached) continue;
                    _dbContext.Entry(entity).State = entity.Id == 0 ? EntityState.Added : EntityState.Modified;
                }
                _dbSet.Attach(item);
            }
            _dbContext.Entry(item).State = EntityState.Modified;
        }

        public TEntity FindByIdWithTrack(int id)
        {
            return _dbSet.FirstOrDefault(item => item.Id == id);
        }

        public IEnumerable<TEntity> GetWithInclude(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return Include(includeProperties);
        }

        public IEnumerable<TEntity> GetWithInclude(Func<TEntity, bool> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var includeResult = Include(includeProperties);
            return includeResult.Where(predicate);
        }

        private IQueryable<TEntity> Include(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = _dbSet.AsNoTracking();
            return includeProperties
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }
    }
}